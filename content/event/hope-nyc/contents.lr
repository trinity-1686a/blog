title: HOPE (NYC)
---
author: steph
---
start_date: 2018-07-20
---
end_date: 2018-07-22
---
body:

The Circle of HOPE will take place on July 20, 21, and 22, 2018 at the [Hotel Pennsylvania in New York City](https://hope.net/travel.html). H.O.P.E. stands for **Hackers On Planet Earth**, one of the most creative and diverse hacker events in the world. It's been happening **since 1994**.

The Tor Project will have a booth for the entire conference and will present "The Onion Report" on Friday at 12:00.

### **The Onion Report**

**[David Goulet](https://hope.net/speakers.html#David%20Goulet), [Alison Macrina](https://hope.net/speakers.html#Alison%20Macrina), [Steph Whited](https://hope.net/speakers.html#Steph%20Whited), [Matthew Finkel](https://hope.net/speakers.html#Matthew%20Finkel)**

The Tor Project has been hard at work building usable free software to fight surveillance and censorship across the globe. Join a handful of Tor contributors at this panel and learn all about the state of the onion and what Tor has been up to since the last HOPE. They’ll talk about adding new security features, improving Tor Browser on Android, deploying the next generation of onion services, making Tor more usable, lowering the network overhead, making Tor more maintainable, and growing the Tor community with new outreach initiatives. They’ll also share some of what you can expect from Tor in the coming year, and will leave lots of time for questions from the community.


title: Reaching People Where They Are
---
pub_date: 2019-09-19
---
author: ggus
---
tags:

tor browser
global south
training
---
categories:

applications
community
---
summary: Part of the Tor Project's mission is to further the popular understanding of privacy technologies, and we believe we can achieve it by combining educational efforts with usability efforts. Popular understanding is to not only be aware of such privacy technologies but to also be able to use and control them. With that in mind, we decided to integrate user experience research into our digital security training with a single program called 'User Feedback Program’.
---
_html_body:

<p>Part of the <strong>Tor Project's mission is to further the popular understanding of privacy technologies</strong>, and we believe we can achieve it by combining educational efforts with usability efforts. Popular understanding is to not only be aware of such privacy technologies but to also be able to use and control them.</p>
<p>With that in mind, we decided to integrate user experience research into our digital security training with a single program called 'User Feedback Program’.</p>
<p><img alt="Tor trainings infographic " src="/static/images/blog/inline-images/tor-trainings_0.png" class="align-center" /></p>
<p>When we first launched this program two years ago, we aimed at a diverse and engaged audience of human rights defenders in the Global South. We are happy to share that, in a moderate estimate, <strong>these activities reached an audience of 800 people through 71 activities in 22 cities and seven countries: Brazil, Colombia, Mexico, India, Indonesia, Kenya, Uganda.</strong></p>
<p>This was our first major effort to establish a training cycle aimed to strengthen and expand the understanding of privacy technologies and the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/global-south">Tor community in the Global South</a>. Members of the UX and Community teams conducted these trainings in partnership with local non-profits, who helped us reach the communities from their countries who needed this type of education the most.</p>
<p>The results of interviews and usability tests were processed and analyzed by our UX team, and their work with users has led to different projects: a collection of personas to help guide our decisions when working on our tools, interface improvements, bugs fixed, and new features already deployed by the Tor Project developers. We are proud to say that today, the Tor Project has a software development cycle that puts the user at the center while respecting privacy.</p>
<p>That is a huge gain, but is not all we set out to do. We learned a lot running these trainings, and we don’t want to keep it to ourselves. We also want to share some insights into our experiences running trainings in the Global South to empower others to do the same. We know we can’t scale if we need to travel to these locations every time a group needs digital security training.</p>
<p>We must share our skills with our partners and help build local capacity that can help these communities on a continuous basis. We are still building this up, and we know we have a long way to go, but we want to start sharing our experiences here with our broader community. We plan on making the slides, training materials, and other resources to run similar trainings available to everyone once we launch our new Community portal, under a special section just for Trainers.</p>
<h2>Inside Tor trainings</h2>
<p>To conduct a training, the Tor Project team consisted of a digital safety trainer and a UX researcher. On average, the trainings took place over 10 days in different cities in a country, where this team traveled and carried out the activities pre-scheduled and promoted by partner NGOs.</p>
<p>At least one threat model exercise was carried out in all cities. Participants identified potential adversaries, attacks, what they want to protect, and how they could do it. This exercise, when played collectively, enables each participant to share and expand their own threat model. Threat model exercises are critical to think about security and were inspired by EFF’s <a href="https://ssd.eff.org/en/module/your-security-plan">Your Security Plan</a>.</p>
<p>The agenda for the day, as well as the content of the training, are customized according to the audience, the number of people, as well as the location. Although we limited the number of training participants to a group of 5 - 25 people, each training had a diverse participation, from human rights defenders, journalists, LGBTQIA+ minorities, political activists, feminists, members of social movements, and non-governmental organizations in the country. In many places, these communities are at risk or under pressure from adversaries on a daily basis. We wanted to strengthen the important work and activism of these organizations by offering the technology we have developed, inviting them to join our community, and trusting in our solidarity.</p>
<p>In some cities, our training was the first time that participants had were introduced to digital security. In regions close to the countryside, there were participants who traveled all night to get to the office of the organization hosting the training. Some of them were peasants who defended the right to produce on their land and wanted to understand the risks of using their mobile phones and how they could communicate in a secure way with other community members.</p>
<p>We also did dedicated trainings for members of specific organizations, and in those cases, we believe it helped strengthen the digital security culture within these organizations. Improving their skills and knowledge on how to protect themselves and bypass censorship whenever necessary.</p>
<p>In trainings customized for journalists, we introduced ways of securely sharing sensitive information using <a href="https://blog.torproject.org/new-release-onionshare-2">OnionShare</a>, <a href="https://blog.torproject.org/tor-heart-onionshare">developed by our collaborator Micah Lee</a>, as well as basic operational security tips.</p>
<p>In places where internet censorship was a reality and the use of VPNs was already part of the user experience, we added as a must-have point of explanation on <a href="https://tb-manual.torproject.org/bridges/">how to use the bridges</a> in Tor Browser to bypass censorship against Tor. A question that arose naturally in almost all training sessions was the difference between Tor and VPNs. We explained that VPNs can be a security bottleneck and must be trusted like a new Internet Service Provider (ISP) who can potentially see all of your traffic. If a VPN says they do not monitor and log your traffic, you have to trust them. That is not the case with Tor. Tor gives privacy by design. We also recommended participants not use a <a href="https://support.torproject.org/faq/faq-5/">VPN with Tor</a>, unless you really know what you're doing.</p>
<p>During the activities, participants asked about the use of other software: <em>Are they safe? Why? </em>We believe that Free/Open Software/Source is an ecosystem in which each project must support and collaborate with each other. And when we talk about digital security, Tor alone is not enough for your security, so we encourage the adoption of tools developed by our colleagues such as <a href="https://guardianproject.info/">Guardian Project </a>and <a href="https://tails.boum.org/">Tails</a>.</p>
<p><em>As we like to say, anonymity loves company. Meeting with human rights defenders and activists around the world was our best company.</em></p>

---
_comments:

<a id="comment-284135"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284135" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 19, 2019</p>
    </div>
    <a href="#comment-284135">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284135" class="permalink" rel="bookmark">the map in tor-trainings…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the map in tor-trainings-lead_0.png is upside down.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-284176"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284176" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 25, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284135" class="permalink" rel="bookmark">the map in tor-trainings…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-284176">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284176" class="permalink" rel="bookmark">it must be on purpose, as…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it must be on purpose, as the words and the positions are right.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-284179"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284179" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">September 25, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284176" class="permalink" rel="bookmark">it must be on purpose, as…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-284179">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284179" class="permalink" rel="bookmark">Yep, it&#039;s a choice on how to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yep, it's a choice on how to represent the world:<br />
<a href="https://en.wikipedia.org/wiki/South-up_map_orientation" rel="nofollow">https://en.wikipedia.org/wiki/South-up_map_orientation</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-284149"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284149" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2019</p>
    </div>
    <a href="#comment-284149">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284149" class="permalink" rel="bookmark">Just wanted to chime in that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Snowden's memoir Permanent Record, MacMillan, 2019, is a stunningly brilliant (and funny!) book, but the relevant points for security trainers are:</p>
<p>Snowden confirms that, long before he learned of the scope of the dragnet, much less that it had turned its malign unblinking pervscanner stare upon the entire body of American people, he tried to get his CIA colleagues in Geneva to use Tor (via Tails) for "open source research", aka Googling the background of persons they HUMINT operatives wanted to pitch.  The official system CIA was using for that was so godawful stupid that... well you have to read the book because you would never believe it if I tried to explain just how godawful stupid it actually was.  Be this as it may, it's quite an endorsement for Tor, and underlies that in 2011 or so, USIC was unable to "break Tor in general" (by connecting specific Tor users with specific exit node streams, or by breaking TLS with perfect forward secrecy).</p>
<p>He mentions using Tails (by name), Shamir's Secret Sharing System (not by name), Secure Drop, and micro SD cards (both by name).  If he had leaked more recently no doubt he would mention OnionShare.  He also puts in a good word for the academic founders of Tor Project :-)</p>
<p>It's an arms race.  Our opponents have sensors, censors, dragnets, nudge programs, trolls, rigged polls, fake news and other influence operations, supercomputers for behavioral modeling and optimizing the government's utility function, helicopters, paramilitaries, tanks, nukes and things, but we also have a few tools which we can use to resist our oppressors.  And at the end of the day, there's a heck of a lot more of us than there are of them.</p>
<p>One of the most powerful weapons of resistance is also one of the most abstract: truth.  We have truth on our side.  That's why our opponents are standing on the wrong side of history.  We can be sure that they will in the fullness of time be swept away--- the only question is whether all humanity will be wiped out along with the bad guys.</p>
<p>I have no financial interest in the book.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284150"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284150" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2019</p>
    </div>
    <a href="#comment-284150">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284150" class="permalink" rel="bookmark">I tried to do some security…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I tried to do some security trainings on an amateur basis and failed.  Later I came across some useful dos and donts (which I hope someone will recognize and post a link to here), two of which I remember and endorse:</p>
<p>o at least two trainers at each event,</p>
<p>o ask questions how the audience uses electronic devices/communications, the obstacles they face, and what they hope to achieve before trying to quickly adapt the general threat modeling process to their needs.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284164"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284164" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2019</p>
    </div>
    <a href="#comment-284164">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284164" class="permalink" rel="bookmark">Please also focus on having…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please also focus on having people in the global south not only use but also run tor nodes.<br />
I've addressed this before but right now the vast majority of usable tor nodes, especially exits are located in the EU or US, virtually none in South America, Africa or Asia. 10 years ago the number of nodes was far more diverse. I understand running an exit node may be dangerous in many countries but there are still a lot of places around the world with both reasonable legal safety and IT infrastructure. People just need to know.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-284166"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284166" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284164" class="permalink" rel="bookmark">Please also focus on having…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-284166">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284166" class="permalink" rel="bookmark">Plus one!
I think the best…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Plus one!</p>
<p>I think the best place to start recruiting exit node operators in Latin America is in the best universities.  Some of them are located in countries which have experienced military dictatorships but which some decades since have returned to democracy, and my impression is that many young CS students are devoted to protecting these democracies.  Ideally TP could inculcate the conviction that operating a Tor node can be a lifelong habit, a very effective way in which individuals can play their part in safeguarding the society in which they live.  And safeguarding civil society anywhere ultimately benefits civil rights everywhere.</p>
<p>(I understand that Derechos Digitales has established ties with civil liberties friendly academic departments in several leading universities in various nations in Latin America.  But I guess that TP already talks to them!)</p>
<p>I hope that eventually there will be sufficiently many exit nodes in Latin America, Africa, and Asia that we can begin to think about another thorny diversity issue: ensuring that there are not so many nodes all located in the same data center that our adversaries might gain the ability to easily deanonymize websurfers using Tor.  But right now it seems the focus should be on getting more exit nodes up and running in regions which are under-represented.</p>
<p>It's always worth remembering that we should encourage new Tor node operators everywhere to consider software diversity, e.g. BSD viz Linux, and some students in CS departments (or former students in certain US universities) might already be confident in their BSD sysadmin skills.</p>
<p>Another point worth stressing is that anyone (with enough bandwidth) can show support by running a nonexit Tor node.  However, as someone recently wrote in another thread, the need for exit nodes is more urgent.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-284167"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284167" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2019</p>
    </div>
    <a href="#comment-284167">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284167" class="permalink" rel="bookmark">To judge from the graphics,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To judge from the graphics, TP is doing a very good job of establishing a foothold in some of the regions where Tor is badly needed.  (Example: Indonesian students are protesting a horrid new criminal code which outlaws adultery and weakens laws against public corruption.)  But to judge from the graphics, one omission is Hong Kong.  Because of GFC, it might not be possible to operate an exit node in HK, but has anyone checked that this is true?   Certainly anyone trying to operate a Tor node in CN would have to be very brave.  I suppose we are all in the frightening position of trying to balance our desire not to recklessly throw away our lives with our hope that courageous action now can ensure a better future for fellow citizens in their teens and twenties, who will have to live for a long time in the world which current elites have created, a world which currently seems to headed down a very disastrous path which could make life very harsh indeed for most people over the next several decades.</p>
</div>
  </div>
</article>
<!-- Comment END -->

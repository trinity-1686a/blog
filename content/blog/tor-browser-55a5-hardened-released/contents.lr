title: Tor Browser 5.5a5-hardened is released
---
pub_date: 2015-12-18
---
author: gk
---
tags:

tor browser
tbb
tbb-5.5
tbb-hardened
---
categories:

applications
releases
---
_html_body:

<p>We are pleased to announce the second release in our hardened Tor Browser series. The download can be found in the <a href="https://dist.torproject.org/torbrowser/5.5a5-hardened/" rel="nofollow">5.5a5-hardened distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-hardened" rel="nofollow">download page for hardened builds</a>.</p>

<p>This release features important <a href="https://www.mozilla.org/en-US/security/known-vulnerabilities/firefox-esr/#firefoxesr38.5" rel="nofollow">security updates</a> to Firefox.</p>

<p>Additionally, we included updated versions for Tor (0.2.7.6), OpenSSL (1.0.1q) and NoScript (2.7). Moreover, we fixed an annoying bug in our circuit display (circuits weren't visible sometimes), isolated SharedWorkers to the first-party domain and improved our font fingerprinting defense.</p>

<p>On the usability side we improved the about:tor experience and started to use the bundled changelog to display new features and bug fixes after an update (instead of loading the blog post into a new tab). We'd love to hear feedback about both.</p>

<p>On the hardening side we are compiling Firefox with -fwrapv now. This is mitigating possible issues with some types of undefined behavior in Mozilla's code.</p>

<p>Tor Browser 5.5a5-hardened comes with a banner supporting our donations campaign. The banner is visible on the about:tor page and features either Roger Dingledine, Laura Poitras or Cory Doctorow which is chosen randomly.</p>

<p><strong>Note:</strong> There are no incremental updates from 5.5a4-hardened available this time due to a <a href="https://bugs.torproject.org/17858" rel="nofollow">bug we detected while building</a>. The internal updater should work, though, doing a complete update.</p>

<p>Here is the complete changelog since 5.5a4-hardened:</p>

<ul>
<li>Update Firefox to 38.5.0esr
   </li>
<li>Update Tor to 0.2.7.6
   </li>
<li> Update OpenSSL to 1.0.1q
   </li>
<li>Update NoScript to 2.7
   </li>
<li>Update Torbutton to 1.9.4.2
<ul>
<li>Bug 16940: After update, load local change notes
       </li>
<li>Bug 16990: Avoid matching '250 ' to the end of node name
       </li>
<li>Bug 17565: Tor fundraising campaign donation banner
       </li>
<li>Bug 17770: Fix alignments on donation banner
       </li>
<li>Bug 17792: Include donation banner in some non en-US Tor Browsers
       </li>
<li>Bug 17108: Polish about:tor appearance
       </li>
<li>Bug 17568: Clean up tor-control-port.js
       </li>
<li>Translation updates
     </li>
</ul>
</li>
<li>Update Tor Launcher to 0.2.8.1
<ul>
<li>Bug 17344: Enumerate available language packs for language prompt
       </li>
<li>Code clean-up
       </li>
<li>Translation updates
     </li>
</ul>
</li>
<li>Bug 12516: Compile Tor Browser with -fwrapv
   </li>
<li>Bug 9659: Avoid loop due to optimistic data SOCKS code (fix of #3875)
   </li>
<li>Bug 15564: Isolate SharedWorkers by first-party domain
   </li>
<li>Bug 16940: After update, load local change notes
   </li>
<li>Bug 17759: Apply whitelist to local fonts in @font-face (fix of #13313)
   </li>
<li>Bug 17747: Add ndnop3 as new default obfs4 bridge
   </li>
<li>Bug 17009: Shift and Alt keys leak physical keyboard layout (fix of #15646)
   </li>
<li>Bug 17369: Disable RC4 fallback
   </li>
<li>Bug 17442: Remove custom updater certificate pinning
   </li>
<li>Bug 16863: Avoid confusing error when loop.enabled is false
   </li>
<li>Bug 17502: Add a preference for hiding "Open with" on download dialog
   </li>
<li>Bug 17446: Prevent canvas extraction by third parties (fixup of #6253)
   </li>
<li>Bug 16441: Suppress "Reset Tor Browser" prompt
  </li>
</ul>

---
_comments:

<a id="comment-142413"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142413" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2015</p>
    </div>
    <a href="#comment-142413">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142413" class="permalink" rel="bookmark">Hi gk
First off, your</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi gk</p>
<p>First off, your contributions to Tor are just awesome.</p>
<p>However as an end user, I am confused as to which version to download: the hardened or the non-hardened one?</p>
<p>Please advise.</p>
<p>P.S. Will using the hardened release <em>break</em> my computer as it might be too hard for my machine to take?...Just kidding.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-142727"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142727" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142413" class="permalink" rel="bookmark">Hi gk
First off, your</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-142727">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142727" class="permalink" rel="bookmark">At present there is no</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>At present there is no stable release of Hardened Tor Browser.  Alpha releases have new features that can be buggier than stable.  In a sense, it may have more security features but those features are more likely to fail.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-142476"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142476" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2015</p>
    </div>
    <a href="#comment-142476">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142476" class="permalink" rel="bookmark">update from 5.0.5</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>update from 5.0.5 FAILS</p>
<p>complains about other versions of firefox.<br />
Cant remove vanilla firefox as CANT access ALL websites with TOR.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-142703"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142703" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142476" class="permalink" rel="bookmark">update from 5.0.5</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-142703">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142703" class="permalink" rel="bookmark">same here especially google</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>same here especially google</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-142479"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142479" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2015</p>
    </div>
    <a href="#comment-142479">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142479" class="permalink" rel="bookmark">Hello there!
Whatever you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello there!<br />
Whatever you enabled on this Version about the Fonts, please enable it it the default-release.</p>
<p>It seems like this release has by default some clear-type-like Font-Randering enabled.<br />
I very much like it!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-142575"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142575" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2015</p>
    </div>
    <a href="#comment-142575">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142575" class="permalink" rel="bookmark">will there eventually be a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>will there eventually be a hardened stable release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-143444"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143444" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 23, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142575" class="permalink" rel="bookmark">will there eventually be a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-143444">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143444" class="permalink" rel="bookmark">Probably not as the hardened</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Probably not as the hardened series is considerably slower and uses considerably more memory than the stable one due to the Address Sanitizer and other additional hardening features.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-143589"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143589" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-143589">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143589" class="permalink" rel="bookmark">I&#039;m downloading Tor Browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm downloading Tor Browser for Mac from <a href="https://www.torproject.org/download/download-easy.html.en#mac" rel="nofollow">https://www.torproject.org/download/download-easy.html.en#mac</a>. I just did it again with the same result.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-142680"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142680" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2015</p>
    </div>
    <a href="#comment-142680">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142680" class="permalink" rel="bookmark">Nice</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nice</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-142686"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142686" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2015</p>
    </div>
    <a href="#comment-142686">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142686" class="permalink" rel="bookmark">Hi,
I have recently noticed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>I have recently noticed something very troubling with TOR, in terms of privacy protection.</p>
<p>I am using using TOR with only the "out of the box" settings.  Looking on the EFF's Panopticlick ( <a href="https://panopticlick.eff.org/" rel="nofollow">https://panopticlick.eff.org/</a> ), as I have done since I began using TOR.  I used to get great results with their testing page, such as out of their 6 million+ database, 1 in 217 browsers have my exact settings.  Which meant, over 27,649 other browsers / users looked like me.  Now however, there are only 19 (nineteen) browsers that also look like mine.  I always check Panoptic everytime I get on TOR.  It's my first stop every session, after verifying that I am in fact using TOR.</p>
<p>However, as of yesterday (and I haven't been on TOR in a few weeks, so not sure when it would have started), Panoptic says my browser has a "nearly unique" fingerprint.</p>
<p>None of my settings have changed, I always deny canvas requests, I'm using TAILS 1.8 (1.7 when Panoptic did this the first time), and I never resize my browser window.</p>
<p>So, what gives? I have tried the tool across multiple sessions, new identities, etc., always the same "nearly unique" fingerprint results. Some sessions produced results that were more unique than others.  Sometimes TOR will block invisible trackers, other times only partially.</p>
<p>Would love to hear anyone's thoughts about this.  Is it an issue with multiple different TOR nodes?  I realize this is not the be-all, but it *is* a marked and troubling result, especially considering this started within mere days of the EFF's Shari Steele coming on as TOR's new executive director. I am not pointing any fingers at Ms. Steele, TOR, the EFF, or anyone else for that matter.  I simply find it very concerning.  Try Panoptic for yourself (as I said, I'm using TAILS), see if you get the same results.</p>
<p>Here is a paste of what I got a moment ago:</p>
<p>A RESEARCH PROJECT OF THE ELECTRONIC FRONTIER FOUNDATION<br />
DONATE<br />
Is your browser safe against tracking?</p>
<p>How well are you protected against non-consensual Web tracking? After analyzing your browser and add-ons, the answer is ...</p>
<p>Mixed results: you have some protection against Web tracking, but it has some gaps. We suggest re-configuring your protection software, or consider installing EFF's Privacy Badger.<br />
Install Privacy Badger<br />
and Enable Do Not Track</p>
<p>Click here for Chrome version<br />
Test    Result<br />
Is your browser blocking tracking ads?  ✓ yes<br />
Is your browser blocking invisible trackers?    ⚠ partial protection<br />
Does your browser unblock 3rd parties that promise to honor Do Not Track?   ✗ no<br />
Does your browser protect from fingerprinting?  ✗<br />
<strong>your browser has a nearly-unique fingerprint</strong></p>
<p>Note: because tracking techniques are complex, subtle, and constantly evolving, Panopticlick does not measure all forms of tracking and protection.</p>
<p>Within our dataset of several million visitors, only one in 310732.4 browsers have the same fingerprint as yours.</p>
<p>Currently, we estimate that your browser has a fingerprint that conveys 18.25 bits of identifying information.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-142782"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142782" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142686" class="permalink" rel="bookmark">Hi,
I have recently noticed</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-142782">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142782" class="permalink" rel="bookmark">Different versions have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Different versions have different fingerprints.  Give some other uses a chance to check out Panopticlick and your rating will get better.<br />
With that said, this is a alpha version of Tor Browser.  It's likely to not have as many users as stable, and is a platform to experiment with new features that may have unintended side effects.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-142904"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142904" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2015</p>
    </div>
    <a href="#comment-142904">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142904" class="permalink" rel="bookmark">Downloaded the latest</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Downloaded the latest version and this is what I get when I try to run it on OS X Yosemite:</p>
<p>"The application “TorBrowser.app” can’t be opened."</p>
<p>It was working fine before the update.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-143445"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143445" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 23, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142904" class="permalink" rel="bookmark">Downloaded the latest</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-143445">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143445" class="permalink" rel="bookmark">What version are you talking</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What version are you talking about (+ which  locale)? The hardened series is Linux 64bit only currently.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-143696"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143696" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-143696">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143696" class="permalink" rel="bookmark">I downloaded the Mac version</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I downloaded the Mac version from here: <a href="https://www.torproject.org/download/download-easy.html.en#mac" rel="nofollow">https://www.torproject.org/download/download-easy.html.en#mac</a></p>
<p>I just tried it again with the same result.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-142916"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142916" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2015</p>
    </div>
    <a href="#comment-142916">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142916" class="permalink" rel="bookmark">okay</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>okay</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-142974"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142974" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2015</p>
    </div>
    <a href="#comment-142974">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142974" class="permalink" rel="bookmark">&gt;The banner is visible on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;The banner is visible on the about:tor page and features either Roger Dingledine, Laura Poitras or Cory Doctorow which is chosen randomly.</p>
<p>How i disable it? This is similar Wikipedia-man with huge eyes, im I worry about this type of banners.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-143786"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143786" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 25, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142974" class="permalink" rel="bookmark">&gt;The banner is visible on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-143786">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143786" class="permalink" rel="bookmark">about:config
extensions.torbu</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>about:config<br />
extensions.torbutton.donation_banner.shown_count -&gt; 99999</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-144229"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-144229" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-143786" class="permalink" rel="bookmark">about:config
extensions.torbu</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-144229">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-144229" class="permalink" rel="bookmark">Thank you, huge eyes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you, huge eyes dissapears :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-143019"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143019" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2015</p>
    </div>
    <a href="#comment-143019">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143019" class="permalink" rel="bookmark">...which PGP/GPG sig are we</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>...which PGP/GPG sig are we supposed to use for this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-143174"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143174" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2015</p>
    </div>
    <a href="#comment-143174">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143174" class="permalink" rel="bookmark">Is tor browser hardened</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is tor browser hardened supposed to hog up a lot of RAM?</p>
<p>Seems to release very little memory although I kill some windows?</p>
<p>The longer I run it the less RAM I have left.<br />
Is there some kind of memory leek?</p>
<p>It ran me dry on RAM and I have no swap so freezed up my system.</p>
<p>Are now running a program monitoring RAM, seems like availible mem<br />
is steadily going down</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-143460"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143460" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 23, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-143174" class="permalink" rel="bookmark">Is tor browser hardened</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-143460">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143460" class="permalink" rel="bookmark">Yes, Address Sanitizer is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, Address Sanitizer is supposed to use a lot of memory. The exact overhead is tricky to determine but up to 3x the amount of stack memory have been reported.</p>
<p>That said I was wondering the same as you and tried to look at this problem a while ago, alas with no definite results. I have opened <a href="https://bugs.torproject.org/17925" rel="nofollow">https://bugs.torproject.org/17925</a> to collect all the findings and keep this on the radar.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-144495"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-144495" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 29, 2015</p>
    </div>
    <a href="#comment-144495">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-144495" class="permalink" rel="bookmark">Is it safe to use the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it safe to use the hardened version or is it not recommended?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-144646"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-144646" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 29, 2015</p>
    </div>
    <a href="#comment-144646">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-144646" class="permalink" rel="bookmark">Is it normal or ok for Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it normal or ok for Tor browser to maintain the same entry node all the time.  I have noticed that my entry node has been persistently the same node (same ip address) no matter the time of day or if I request a new Tor server node.  I find this troubling</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-144848"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-144848" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 30, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-144646" class="permalink" rel="bookmark">Is it normal or ok for Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-144848">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-144848" class="permalink" rel="bookmark">It&#039;s</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's normal:<br />
<a href="https://www.torproject.org/docs/faq#EntryGuards" rel="nofollow">https://www.torproject.org/docs/faq#EntryGuards</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-146872"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-146872" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-146872">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-146872" class="permalink" rel="bookmark">The FAQ talks about multiple</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The FAQ talks about multiple nodes, not a single one that monitors us for a long (how long actually?) period of time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>

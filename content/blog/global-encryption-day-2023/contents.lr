title: Global Encryption Day: Encryption's Critical Role in Safeguarding Human Rights
---
author: pavel
---
pub_date: 2023-10-21
---
categories: advocacy
---
summary:
Today is the third annual [Global Encryption Day](https://www.globalencryption.org/2023/08/global-encryption-day-2023/), a day to highlight why encryption is critical for a safe, private internet.
---
body:
Today, October 21, 2023, is the third [Global Encryption Day](https://blog.torproject.org/global-encryption-day-2022/), organized by the [Global Encryption Coalition](https://www.globalencryption.org/), of which the Tor Project is a member. Global Encryption Day is an opportunity for businesses, civil society organizations, technologists, and millions of Internet users worldwide to highlight why encryption matters and to advocate for its advancement and protection.

Needless to say, fighting for the protection of encryption is a topic for us 365 days a year. But we want to use this day to reflect on some of the efforts we have initiated or supported in 2023 to [ensure access to encryption](https://blog.torproject.org/how-we-plant-and-grow-new-onions/) and push back against [government efforts that seek to undermine it](https://www.badinternetbills.com/) - from [signing letters](https://www.globalencryption.org/2023/06/a-call-to-the-brazilian-supreme-court-to-recognize-the-importance-of-encryption-in-safeguarding-fundamental-rights/#:~:text=By%20taking%20these%20measures%2C%20the,for%20secure%20and%20confidential%20communication.) and supporting ally organizations on a number of advocacy and [awareness campaigns](https://www.univision.com/local/san-francisco-kdtv/protesta-slack-cifrado-extremo-mensajes-abortos-fotos) to expanding our [outreach and training efforts](https://blog.torproject.org/empowering-human-rights-defenders/) or [localizing educational content](https://blog.torproject.org/tor-tutorials/).

## Encryption's Critical Role in Safeguarding Human Rights

Most recently, we co-organized and co-hosted a workshop at the Internet Governance Forum (IGF) - an annual meeting established by the United Nations in 2005 as a forum for multi-stakeholder dialogue concerning public policy issues relating to the internet. The panel entitled [Encryption's Critical Role in Safeguarding Human Rights](https://www.intgovforum.org/en/content/igf-2023-ws-356-encryptions-critical-role-in-safeguarding-human-rights) brought together professionals from the technology, non-profit, policy, human rights and advocacy spaces to discuss balancing the demands of national security with the protection of individual privacy, adherence to international human rights laws and seeking a global approach.

The debate underscored the importance of international collaboration and respecting international standards and human rights principles to navigate the complexities of encryption and content moderation, pushing back against the weakening of encryption and the proliferation of surveillance technologies that have been used against human rights activists, journalists, and members of the civil society. The panelists called for education, including training and capacity-building of policymakers, and the normalization of encryption in creating a safer online environment for all users. Tech companies must play a role in protecting user privacy while addressing legitimate concerns related to content moderation, including the ability for safe and responsive reporting channels for users that do not compromise encryption and acknowledging the limitations of AI. By offering end-to-end encryption they can give power back to the users and simultaneously simplify their regulatory burden.

Two key takeaways and calls for action from our discussion have already been reflected in the first draft of IGF messaging outcomes ["intended to provide a high-level overview for decision-makers of the most current thinking on key Internet governance and digital policy issues."](https://intgovforum.org/en/content/kyoto-messages)

### HUMAN RIGHTS AND FREEDOMS

+ "Policymakers need to improve their understanding of Internet technologies, the infrastructure underpinning them, their modalities and business models if they are to make informed policy decisions and design appropriate regulatory frameworks. Greater transparency on the part of businesses and other stakeholders can help to achieve this."

+ "Technology is not confined by geographic boundaries. Laws and regulations governing the use of technology in areas such as encryption should be consistent with international standards and norms concerned with privacy, freedom of expression, due process and access to information."

If you're interested to find out what other measures and recommendations to policymakers were proposed, check out the full session recording [here](https://www.youtube.com/watch?v=M4FgMt3KXec).

The Tor Project's mission is to advance Human Rights. For us that means not only by building and deploying technology, but also by advocating for the protection and unrestricted availability of privacy-preserving technologies like encryption. We are connecting with people all around the world and supporting them to use the internet safely and freely to achieve this mission.

___

## Ways to contribute

If you're reading this and [want to support the work we are doing to ensure that the Tor Project remains strong](https://blog.torproject.org/2023-fundraiser-make-a-donation-to-Tor/) on an organizational level, and that the ecosystem of Tor services and tools continue to reach the people who need privacy online the most, [please consider making a donation today](https://donate.torproject.org/).

* **Can you make a recurring donation?** [Set up a monthly donation](https://torproject.org/donate/donate-bp1-2023). This is the best, most impactful way to help the Tor Project budget during uncertain times.

* **Can you make a one-time gift?** Make a [donation through our website by credit card or PayPal](https://torproject.org/donate/donate-bp1-2023); through the mail by cash, check, or money order; through a stock donation or bank transfer; and [many more options listed in our FAQ](https://donate.torproject.org/donor-faq/).

* **Do you have a company or organization that uses Tor or believes in Tor?** [Become a member](https://torproject.org/about/membership).

* **Does your company match donations made by employees?** Make a donation and file the paperwork with your organization so they match your gift. Let us know if you need help!

* **Do you have cryptocurrency to give?** [We can accept donations in ten different coins](https://donate.torproject.org/cryptocurrency).




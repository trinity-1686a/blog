title: Arti 1.1.4 is released: Development on Onion Services and RPC
---
author: nickm
---
pub_date: 2023-05-03
---
categories: announcements
---
summary:

Arti 1.1.4 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.1.4.

For this month and the next,
our efforts are divided between onion services
and work on a new RPC API
(a successor to C Tor's "control port")
that will give applications a safe and powerful way
to work with Arti
without having to write their code in Rust
or link Arti as a library (unless they want to).

(This release is a little smaller than usual,
since many of us spent a week at an in-person meeting.)

For onion services,
we have implemented the client side
of the service descriptor downloading,
including support for finding descriptors on the `HSDir` ring.

Our RPC code is still in an "infrastructure-only" state:
the backend is mostly built,
but as of yet it supports no useful functionality.
For information on the general shape of our design,
see the work-in-progress [specification document].

This release also solves a bug
that prevented directories from updating under some circumstances.

There have been many smaller changes as well;
for those, please see the [CHANGELOG].

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everyone who has contributed to this release, including
Alexander Færøy, juga, Neel Chauhan, tranna, and Trinity Pointard.

Finally, our deep thanks to [Zcash Community Grants] for funding the
development of Arti!


[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-114-3-may-2023
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[specification document]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/doc/dev/notes/rpc-meta-draft.md

title: Global Encryption Day: Demand End-to-End Encryption in DMs
---
author: alsmith
---
pub_date: 2022-10-21
---
categories: advocacy
---
summary:
Today is [Global Encryption Day](https://www.globalencryption.org/2022/06/global-encryption-day-2022/), a day to highlight why encryption is critical for a safe, private internet.
---
body:
Today, October 21, 2022, is the second [Global Encryption Day](https://www.globalencryption.org/2022/06/global-encryption-day-2022/), organized by the Global Encryption Coalition, of which the Tor Project is a member. Global Encryption Day is an opportunity for businesses, civil society organizations, technologists, and millions of Internet users worldwide to highlight why encryption matters and to advocate for its advancement and protection. 

At the Tor Project, we’re proud to help millions of people take back their right to privacy, to freely access and share information, and to more easily circumvent internet censorship--and encryption makes this possible.

Encryption allows us to provide these tools: for example, Tor uses three layers of encryption in the Tor circuit; each relay decrypts one layer before passing the request on to the next relay. Encryption is used in many other ways as well! Without encryption, millions of people would lose their access to the safe and uncensored internet. 

Global Encryption Day is also a day for all of us to highlight services that *should* be encrypted--but are not yet.

The context of our online lives changes from day-to-day. Conversations that seem safe one day may contain illegal content the next. In the U.S., after the reversal of Roe v. Wade in June 2022, platforms have been forced to turn over messages between users that took place *before* June. For example, [Facebook](https://www.forbes.com/sites/emilybaker-white/2022/08/08/facebook-abortion-teen-dms/) recently handed over direct messages between a mom and her teenage daughter to police that took place in April. Without end-to-end encryption, our online lives can be hacked, shared with law enforcement, or even accessed by creepy company employees.

**Private and secure spaces for online communication and support are more important right now than ever.**

That's why this Global Encryption Day, we’re calling on platforms to do what’s right: protect our privacy. We’re demanding that Big Tech finally bring the protection of end-to-end encryption to DMs in their products--not as a hidden “added feature”--but as the default setting. Companies like Meta, Twitter, Apple, Google, Slack, Discord, and all services with chat features must add end-to-end encryption their messaging services immediately.

Big Tech needs to make DMs safe **now.** We've signed an open letter to MAKE DMS SAFE organized by Fight for the Future, and we encourage you to do the same. [Demand that Facebook, Twitter, Google, Apple--and any company with a messaging platform-- protect our private messages and implement default end-to-end encryption immediately](https://www.makedmssafe.com/).
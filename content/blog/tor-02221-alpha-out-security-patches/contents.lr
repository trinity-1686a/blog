title: Tor 0.2.2.21-alpha is out (security patches)
---
pub_date: 2011-01-19
---
author: erinn
---
tags:

tor
tor browser bundle
alpha release
security fixes
updated packages
tbb
---
categories:

applications
network
releases
---
_html_body:

<p>Note to <strong>64-bit Linux Tor Browser Bundle</strong> users: The previous bundles contained Tor 0.2.2.20-alpha. Please upgrade to <a href="https://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-i686-1.1.3-dev-en-US.tar.gz" rel="nofollow">1.1.3-1</a> (<a href="https://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-i686-1.1.3-dev-en-US.tar.gz.asc" rel="nofollow">sig</a>).</p>

<p>Tor 0.2.2.21-alpha includes all the patches from Tor 0.2.1.29, which<br />
continues our recent code security audit work. The main fix resolves<br />
a remote heap overflow vulnerability that can allow remote code<br />
execution (CVE-2011-0427). Other fixes address a variety of assert<br />
and crash bugs, most of which we think are hard to exploit remotely.</p>

<p><strong>All Tor users should upgrade.</strong></p>

<p><a href="https://www.torproject.org/download/download" rel="nofollow">https://www.torproject.org/download/download</a></p>

<p>Changes in version 0.2.2.21-alpha - 2011-01-15<br />
<strong>Major bugfixes (security), also included in 0.2.1.29:</strong></p>

<ul>
<li>Fix a heap overflow bug where an adversary could cause heap<br />
      corruption. This bug probably allows remote code execution<br />
      attacks. Reported by "debuger". Fixes CVE-2011-0427. Bugfix on<br />
      0.1.2.10-rc.</li>
<li>Prevent a denial-of-service attack by disallowing any<br />
      zlib-compressed data whose compression factor is implausibly<br />
      high. Fixes part of bug 2324; reported by "doorss".</li>
<li>Zero out a few more keys in memory before freeing them. Fixes<br />
      bug 2384 and part of bug 2385. These key instances found by<br />
      "cypherpunks", based on Andrew Case's report about being able<br />
      to find sensitive data in Tor's memory space if you have enough<br />
      permissions. Bugfix on 0.0.2pre9.</li>
</ul>

<p><strong>Major bugfixes (crashes), also included in 0.2.1.29:</strong></p>

<ul>
<li>Prevent calls to Libevent from inside Libevent log handlers.<br />
      This had potential to cause a nasty set of crashes, especially<br />
      if running Libevent with debug logging enabled, and running<br />
      Tor with a controller watching for low-severity log messages.<br />
      Bugfix on 0.1.0.2-rc. Fixes bug 2190.</li>
<li>Add a check for SIZE_T_MAX to tor_realloc() to try to avoid<br />
      underflow errors there too. Fixes the other part of bug 2324.</li>
<li>Fix a bug where we would assert if we ever had a<br />
      cached-descriptors.new file (or another file read directly into<br />
      memory) of exactly SIZE_T_CEILING bytes. Fixes bug 2326; bugfix<br />
      on 0.2.1.25. Found by doorss.</li>
<li>Fix some potential asserts and parsing issues with grossly<br />
      malformed router caches. Fixes bug 2352; bugfix on Tor 0.2.1.27.<br />
      Found by doorss.</li>
</ul>

<p><strong>Minor bugfixes (other), also included in 0.2.1.29:</strong></p>

<ul>
<li>Fix a bug with handling misformed replies to reverse DNS lookup<br />
      requests in DNSPort. Bugfix on Tor 0.2.0.1-alpha. Related to a<br />
      bug reported by doorss.</li>
<li>Fix compilation on mingw when a pthreads compatibility library<br />
      has been installed. (We don't want to use it, so we shouldn't<br />
      be including pthread.h.) Fixes bug 2313; bugfix on 0.1.0.1-rc.</li>
<li>Fix a bug where we would declare that we had run out of virtual<br />
      addresses when the address space was only half-exhausted. Bugfix<br />
      on 0.1.2.1-alpha.</li>
<li>Correctly handle the case where AutomapHostsOnResolve is set but<br />
      no virtual addresses are available. Fixes bug 2328; bugfix on<br />
      0.1.2.1-alpha. Bug found by doorss.</li>
<li>Correctly handle wrapping around when we run out of virtual<br />
      address space. Found by cypherpunks; bugfix on 0.2.0.5-alpha.</li>
</ul>

<p><strong>Minor features, also included in 0.2.1.29:</strong></p>

<ul>
<li>Update to the January 1 2011 Maxmind GeoLite Country database.</li>
<li>Introduce output size checks on all of our decryption functions.</li>
</ul>

<p><strong>Build changes, also included in 0.2.1.29:</strong></p>

<ul>
<li>Tor does not build packages correctly with Automake 1.6 and earlier;<br />
      added a check to Makefile.am to make sure that we're building with<br />
      Automake 1.7 or later.</li>
<li>The 0.2.1.28 tarball was missing src/common/OpenBSD_malloc_Linux.c<br />
      because we built it with a too-old version of automake. Thus that<br />
      release broke ./configure --enable-openbsd-malloc, which is popular<br />
      among really fast exit relays on Linux.</li>
</ul>

<p><strong>Major bugfixes, new in 0.2.2.21-alpha:</strong></p>

<ul>
<li>Prevent crash/heap corruption when the cbtnummodes consensus<br />
      parameter is set to 0 or large values. Fixes bug 2317; bugfix<br />
      on 0.2.2.14-alpha.</li>
</ul>

<p><strong>Major features, new in 0.2.2.21-alpha:</strong></p>

<ul>
<li>Introduce minimum/maximum values that clients will believe<br />
      from the consensus. Now we'll have a better chance to avoid crashes<br />
      or worse when a consensus param has a weird value.</li>
</ul>

<p><strong>Minor features, new in 0.2.2.21-alpha:</strong></p>

<ul>
<li>Make sure to disable DirPort if running as a bridge. DirPorts aren't<br />
      used on bridges, and it makes bridge scanning somewhat easier.</li>
<li>If writing the state file to disk fails, wait up to an hour before<br />
      retrying again, rather than trying again each second. Fixes bug<br />
      2346; bugfix on Tor 0.1.1.3-alpha.</li>
<li>Make Libevent log messages get delivered to controllers later,<br />
      and not from inside the Libevent log handler. This prevents unsafe<br />
      reentrant Libevent calls while still letting the log messages<br />
      get through.</li>
<li>Detect platforms that brokenly use a signed size_t, and refuse to<br />
      build there. Found and analyzed by doorss and rransom.</li>
<li>Fix a bunch of compile warnings revealed by mingw with gcc 4.5.<br />
      Resolves bug 2314.</li>
</ul>

<p><strong>Minor bugfixes, new in 0.2.2.21-alpha:</strong></p>

<ul>
<li>Handle SOCKS messages longer than 128 bytes long correctly, rather<br />
      than waiting forever for them to finish. Fixes bug 2330; bugfix<br />
      on 0.2.0.16-alpha. Found by doorss.</li>
<li>Add assertions to check for overflow in arguments to<br />
      base32_encode() and base32_decode(); fix a signed-unsigned<br />
      comparison there too. These bugs are not actually reachable in Tor,<br />
      but it's good to prevent future errors too. Found by doorss.</li>
<li>Correctly detect failures to create DNS requests when using Libevent<br />
      versions before v2. (Before Libevent 2, we used our own evdns<br />
      implementation. Its return values for Libevent's evdns_resolve_*()<br />
      functions are not consistent with those from Libevent.) Fixes bug<br />
      2363; bugfix on 0.2.2.6-alpha. Found by "lodger".</li>
</ul>

<p><strong>Documentation, new in 0.2.2.21-alpha:</strong></p>

<ul>
<li>Document the default socks host and port (127.0.0.1:9050) for<br />
      tor-resolve.</li>
</ul>


title: New Release: Tor 0.3.5.5-alpha
---
pub_date: 2018-11-16
---
author: nickm
---
tags: alpha release
---
categories: releases
---
summary: Tor 0.3.5.5-alpha includes numerous bugfixes on earlier releases, including several that we hope to backport to older release series in the future.
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.3.5.5-alpha from the download page on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release by late next week.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.3.5.5-alpha includes numerous bugfixes on earlier releases, including several that we hope to backport to older release series in the future.</p>
<h2>Changes in version 0.3.5.5-alpha - 2018-11-16</h2>
<ul>
<li>Major bugfixes (OpenSSL, portability):
<ul>
<li>Fix our usage of named groups when running as a TLS 1.3 client in OpenSSL 1.1.1. Previously, we only initialized EC groups when running as a relay, which caused clients to fail to negotiate TLS 1.3 with relays. Fixes bug <a href="https://bugs.torproject.org/28245">28245</a>; bugfix on 0.2.9.15 (when TLS 1.3 support was added).</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the November 6 2018 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/28395">28395</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (compilation):
<ul>
<li>Initialize a variable unconditionally in aes_new_cipher(), since some compilers cannot tell that we always initialize it before use. Fixes bug <a href="https://bugs.torproject.org/28413">28413</a>; bugfix on 0.2.9.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (connection, relay):
<ul>
<li>Avoid a logging a BUG() stacktrace when closing connection held open because the write side is rate limited but not the read side. Now, the connection read side is simply shut down until Tor is able to flush the connection and close it. Fixes bug <a href="https://bugs.torproject.org/27750">27750</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (continuous integration, Windows):
<ul>
<li>Manually configure the zstd compiler options, when building using mingw on Appveyor Windows CI. The MSYS2 mingw zstd package does not come with a pkg-config file. Fixes bug <a href="https://bugs.torproject.org/28454">28454</a>; bugfix on 0.3.4.1-alpha.</li>
<li>Stop using an external OpenSSL install, and stop installing MSYS2 packages, when building using mingw on Appveyor Windows CI. Fixes bug <a href="https://bugs.torproject.org/28399">28399</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (documentation):
<ul>
<li>Make Doxygen work again after the code movement in the 0.3.5 source tree. Fixes bug <a href="https://bugs.torproject.org/28435">28435</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (Linux seccomp2 sandbox):
<ul>
<li>Permit the "shutdown()" system call, which is apparently used by OpenSSL under some circumstances. Fixes bug <a href="https://bugs.torproject.org/28183">28183</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Stop talking about the Named flag in log messages. Clients have ignored the Named flag since 0.3.2. Fixes bug <a href="https://bugs.torproject.org/28441">28441</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (memory leaks):
<ul>
<li>Fix a harmless memory leak in libtorrunner.a. Fixes bug <a href="https://bugs.torproject.org/28419">28419</a>; bugfix on 0.3.3.1-alpha. Patch from Martin Kepplinger.</li>
</ul>
</li>
<li>Minor bugfixes (onion services):
<ul>
<li>On an intro point for a version 3 onion service, stop closing introduction circuits on an NACK. This lets the client decide whether to reuse the circuit or discard it. Previously, we closed intro circuits when sending NACKs. Fixes bug <a href="https://bugs.torproject.org/27841">27841</a>; bugfix on 0.3.2.1-alpha. Patch by Neel Chaunan.</li>
<li>When replacing a descriptor in the client cache, make sure to close all client introduction circuits for the old descriptor, so we don't end up with unusable leftover circuits. Fixes bug <a href="https://bugs.torproject.org/27471">27471</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-278501"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278501" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 16, 2018</p>
    </div>
    <a href="#comment-278501">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278501" class="permalink" rel="bookmark">Should we ask Tor Browser…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Should we ask Tor Browser Team to switch to TLS 1.3 client with OpenSSL 1.1.1?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278598"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278598" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">November 26, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278501" class="permalink" rel="bookmark">Should we ask Tor Browser…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278598">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278598" class="permalink" rel="bookmark">They&#039;ll get there eventually…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They'll get there eventually. For now, I think it's a better idea to go slowly and give more people time to find bugs.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278603"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278603" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 26, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to nickm</p>
    <a href="#comment-278603">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278603" class="permalink" rel="bookmark">Then why do you allow Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Then why do you allow Tor clients to connect with TLS 1.3 by default? Isn't it weird to have two types of clients/traffic in the Tor network?<br />
Also, as it is a security upgrade, shouldn't you prioritize this work and recommend to switch to TLS 1.3 as OpenSSL team does?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278619"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278619" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">November 27, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278603" class="permalink" rel="bookmark">Then why do you allow Tor…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278619">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278619" class="permalink" rel="bookmark">Two types of travel relayed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Two types of travel relayed across the network would be a fingerprinting risk.  But TLS1.3 only affects the traffic between the client and the guard.</p>
<p>When it comes to upgrading, we need to balance prioritizing upgrades and prioritizing stability.  New software tends to have more bugs than older versions.  For instance, here's a bug in OpenSSL 1.1.1a that breaks our handshakes: <a href="https://trac.torproject.org/projects/tor/ticket/28616" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/28616</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-278504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278504" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 17, 2018</p>
    </div>
    <a href="#comment-278504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278504" class="permalink" rel="bookmark">(#1) Error   Killing GPU…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>(#1) Error   Killing GPU process due to IPC reply timeout<br />
(#2) Error  Failed to connect GPU process<br />
(#3) Error  Receive IPC close with reason=AbnormalShutdown</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278599"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278599" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">November 26, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278504" class="permalink" rel="bookmark">(#1) Error   Killing GPU…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278599">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278599" class="permalink" rel="bookmark">This sounds more like a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This sounds more like a Torbrowser issue than a Tor issue -- Tor doesn't mess with the GPU.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278513"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278513" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>c (not verified)</span> said:</p>
      <p class="date-time">November 18, 2018</p>
    </div>
    <a href="#comment-278513">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278513" class="permalink" rel="bookmark">could tor consider add a f2f…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>could tor consider add a f2f layer with obfs bridges? not like now, but add a opition for the user in the region can connect to tor network directly being a bridge, and broadcast it via DHT not a server. you know that china can block all bridge ip, they are in the central server, but if the ip is anyone's home ip, they can't block the dymanic ips. and just be a entry for the network, they will not be in a legal risk.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278521"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278521" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2018</p>
    </div>
    <a href="#comment-278521">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278521" class="permalink" rel="bookmark">This is not related to this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is not related to this release, but I fear that nobody would notice me in the older topics before the next TB release.</p>
<p>DuckDuckGo.com often blocks random exit nodes. It's really annoying sometimes. I suggest that Tor Browser moves to the official DuckDuckGo hidden service: <a href="http://3g2upl4pq6kufc4m.onion" rel="nofollow">http://3g2upl4pq6kufc4m.onion</a> (address can be verified by searching "duckduckgo onion" on ddg.gg)</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278535"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278535" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">November 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278521" class="permalink" rel="bookmark">This is not related to this…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278535">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278535" class="permalink" rel="bookmark">If that&#039;s a DuckDuckGo issue…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If that's a DuckDuckGo issue, please contact them so they can fix that. We don't have plans to use the onion services by default right now but it's an options for you to switch to.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278588"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278588" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>chlars (not verified)</span> said:</p>
      <p class="date-time">November 25, 2018</p>
    </div>
    <a href="#comment-278588">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278588" class="permalink" rel="bookmark">无法使用代理连接，内置网桥可以连接
连接过程中会断网</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>无法使用代理连接，内置网桥可以连接<br />
连接过程中会断网</p>
</div>
  </div>
</article>
<!-- Comment END -->

title: New Alpha Release: Tor Browser 12.5a7 (Android, Windows, macOS, Linux)
---
pub_date: 2023-06-09
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 12.5a7 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 12.5a7 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/12.5a7/).

This release updates Firefox to 102.12.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-19/). We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-20/) from Firefox 114.

We would like to thank volunteer contributor cypherpunks1 for their fixes for [tor-browser#33298](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/33298), [tor-browser#41792](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41792), and [tor-browser#41785](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41785). If you would like to contribute, our issue tracker can be found [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/boards/1243#Platform).

This should be the last alpha in the 12.5 series, so this release should be considered a release candidate for 12.5.0 which will be released in the coming weeks. If you find any issues, please report them on our [gitlab](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/new) or on the [Tor Project forum](https://forum.torproject.net/c/feedback/tor-browser-alpha-feedback/6).

## Build-Signing Infrastructure Updates

We are once again able to code-sign our executable Windows installer, so new installations on the Windows platform no longer need to perform a build-to-build update from an older version. We apologize for all the inconvenience this caused.

## Full changelog

The full changelog since [Tor Browser 12.5a6](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated NoScript to 11.4.22
  - Updated OpenSSL to 1.1.1u
  - [Bug tor-browser#41795](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41795): Rebase Tor Browser and Base Browser alpha to 102.12esr
  - [Bug tor-browser#41818](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41818): Remove YEC 2022 strings
- Windows + macOS + Linux
  - Updated Firefox to 102.12esr
  - [Bug tor-browser#33298](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/33298): HTTP onion sites do not give a popup warning when submitting form data to non-onion HTTP sites
  - [Bug tor-browser#40552](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40552): New texts for the add a bridge manually modal
  - [Bug tor-browser#41608](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41608): Improve the UX of the location bar's connection status
  - [Bug tor-browser#41618](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41618): Update the iconography used in the status strip in connection settings
  - [Bug tor-browser#41623](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41623): Update connection assist's iconography
  - [Bug tor-browser#41718](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41718): Add the external filetype warning to about:downloads
  - [Bug tor-browser#41726](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41726): Animate the torconnect icon to transition between connected states
  - [Bug tor-browser#41734](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41734): Add a `Connected` flag to indicate which built-in bridge option Tor Browser is currently using
  - [Bug tor-browser#41749](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41749): Replace the onion-glyph with dedicated icon for onion services
  - [Bug tor-browser#41785](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41785): Network monitor in developer tools shows HTTP onion resources as insecure
  - [Bug tor-browser#41792](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41792): Drag and Drop protection prevents dragging downloads
  - [Bug tor-browser#41800](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41800): Add the external filetype warning to Library / Manage Bookmarks
  - [Bug tor-browser#41801](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41801): Fix handleProcessReady in TorSettings.init
  - [Bug tor-browser#41802](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41802): Conjure bridge cards are mislabeled as vanilla bridges in alpha
  - [Bug tor-browser#41809](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41809): Wrong icon in the bridge QR code
  - [Bug tor-browser#41810](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41810): Add "Connect" buttons to Request Bridge and Provide Bridge modals
  - [Bug tor-browser#41815](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41815): wrong connect icons
  - [Bug tor-browser#41816](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41816): The top navigation in about:torconnect isn't updated correctly
- Android
  - Updated GeckoView to 102.12esr
- Build System
  - All Platforms
    - [Bug tor-browser-build#40777](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40777): Create a Go bootstrap project
    - [Bug tor-browser-build#40850](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40850): Tor Browser nightly fails to build obfs4
    - [Bug tor-browser-build#40866](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40866): Remove `Using ansible to set up a nightly build machine` from README
    - [Bug tor-browser-build#40869](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40869): obfs4 is renamed to lyrebird
    - [Bug tor-browser-build#40870](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40870): Remove url without browser name from tools/signing/download-unsigned-sha256sums-gpg-signatures-from-people-tpo
    - [Bug tor-browser-build#40871](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40871): Update keyring/boklm.gpg for new subkeys
  - Windows + macOS + Linux
    - [Bug tor-browser-build#40864](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40864): Make a script to update the manual artifact
  - macOS
    - [Bug tor-browser-build#40847](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40847): Build filesystem influences the DMG creation
    - [Bug tor-browser-build#40858](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40858): Create script to assist testers self sign Mac builds to allow running on Arm processors
  - Android
    - [Bug tor-browser-build#40874](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40874): Add commit information also to GV

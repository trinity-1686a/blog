title: Tor Browser 7.5a7 is released
---
pub_date: 2017-11-04
---
author: boklm
---
tags:

tor browser
tbb
tbb-7.5
---
categories: applications
---
_html_body:

<p><strong>Note: Tor Browser 7.5a7 is a security bugfix release in the alpha channel for macOS and Linux users only. Users of the alpha channel on Windows are not affected and stay on Tor Browser 7.5a6.</strong></p>
<p>Tor Browser 7.5a7 is now available for our macOS and Linux users from the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha">Tor Browser Project page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/7.5a7/">distribution directory</a>.</p>
<p>This release features an important security update to Tor Browser for macOS and Linux users. Due to a <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1412081">Firefox bug</a> in handling file:// URLs it is possible on both systems that users leak their IP address. Once an affected user navigates to a specially crafted URL the operating system may directly connect to the remote host, bypassing Tor Browser. <strong>Tails users and users of our sandboxed-tor-browser are unaffected, though</strong>.</p>
<p>The bug got reported to us on Thursday, October 26, by Filippo Cavallarin. We created a workaround with the help of Mozilla engineers on the next day which, alas, fixed the leak only partially. We developed an additional fix on Tuesday, October 31, plugging all known holes. We are not aware of this vulnerability being exploited in the wild. Thanks to everyone who helped during this process!</p>
<p><strong>Known issues</strong>: The fix we deployed is just a workaround stopping the leak. As a result of that navigating file:// URLs in the browser might not work as expected anymore. In particular entering file:// URLs in the URL bar and clicking on resulting links is broken. Opening those in a new tab or new window does not work either. A workaround for those issues is dragging the link into the URL bar or on a tab instead. We track this follow-up regression in <a href="https://trac.torproject.org/projects/tor/ticket/24136">bug 24136</a>.</p>
<p>Here is the full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt">changelog</a> since 7.5a6:</p>
<ul>
<li>OS X
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/24052">Bug 24052</a>: Streamline handling of file:// resources</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/24052">Bug 24052</a>: Streamline handling of file:// resources</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-272352"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272352" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2017</p>
    </div>
    <a href="#comment-272352">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272352" class="permalink" rel="bookmark">Thank you &lt;3 !! :)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you &lt;3 !! :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272361"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272361" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>me (not verified)</span> said:</p>
      <p class="date-time">November 04, 2017</p>
    </div>
    <a href="#comment-272361">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272361" class="permalink" rel="bookmark">whats with the:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>whats with the:</p>
<p>Changelog:<br />
Tor Browser 7.5a7 -- November 6 2017<br />
 * OS X<br />
   * Bug 24052: Streamline handling of file:// resources<br />
 * Linux<br />
   * Bug 24052: Streamline handling of file:// resources</p>
<p>This is November 4th, even with the international date line it doesn't work.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272362"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272362" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">November 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272361" class="permalink" rel="bookmark">whats with the:…</a> by <span>me (not verified)</span></p>
    <a href="#comment-272362">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272362" class="permalink" rel="bookmark">The process to prepare a new…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The process to prepare a new release takes a few days, but the Changelog is written when we start the build so we have to guess when everything will be ready for publishing, and this time we were faster than we planned.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272367"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272367" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2017</p>
    </div>
    <a href="#comment-272367">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272367" class="permalink" rel="bookmark">oooo</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>oooo</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272375"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272375" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2017</p>
    </div>
    <a href="#comment-272375">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272375" class="permalink" rel="bookmark">Really proud that you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Really proud that you thought about us, alpha users, thanks tor browser team :D</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272391"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272391" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Richrd (not verified)</span> said:</p>
      <p class="date-time">November 06, 2017</p>
    </div>
    <a href="#comment-272391">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272391" class="permalink" rel="bookmark">Friends, do this bug affect…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Friends, do this bug affect Firefox Portable when configured with Tor expert bundle and running inside Windows?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272396"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272396" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">November 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272391" class="permalink" rel="bookmark">Friends, do this bug affect…</a> by <span>Richrd (not verified)</span></p>
    <a href="#comment-272396">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272396" class="permalink" rel="bookmark">This bug does not affect…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This bug does not affect Windows users. However you should not expect to be really anonymous if you are using a normal Firefox configured to use Tor. The Tor Browser includes many privacy features which are not included in normal browsers: <a href="https://www.torproject.org/projects/torbrowser/design/" rel="nofollow">https://www.torproject.org/projects/torbrowser/design/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272418"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272418" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>question (not verified)</span> said:</p>
      <p class="date-time">November 07, 2017</p>
    </div>
    <a href="#comment-272418">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272418" class="permalink" rel="bookmark">What hapens when Entry guard…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What happens when Entry guard or Middle relay is trying to control, to decide which following relay it will allow?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272467"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272467" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Max (not verified)</span> said:</p>
      <p class="date-time">November 11, 2017</p>
    </div>
    <a href="#comment-272467">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272467" class="permalink" rel="bookmark">Хек</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Хек</p>
</div>
  </div>
</article>
<!-- Comment END -->

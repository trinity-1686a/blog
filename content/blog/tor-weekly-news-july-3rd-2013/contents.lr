title: Tor Weekly News — July 3rd, 2013
---
pub_date: 2013-07-03
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the very first issue of Tor Weekly News, the weekly newsletter meant to cover what is happening in the vibrant Tor community.</p>

<h1>Deterministic, independently reproduced builds of Tor Browser Bundle</h1>

<p>Mike Perry, Linus Nordberg and Georg Koppen each independently built identical binaries of the <a href="https://blog.torproject.org/blog/tor-browser-bundle-30alpha2-released" rel="nofollow">Tor Browser Bundle 3.0 alpha 2 release</a>, now <a href="https://archive.torproject.org/tor-package-archive/torbrowser/3.0a2/" rel="nofollow">available for download at the Tor Package Archive</a>.</p>

<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/README.build" rel="nofollow">build system</a>, first adopted for the release of 3.0 alpha 1, uses <a href="http://gitian.org/" rel="nofollow">Gitian</a> to enable anyone to produce byte-identical Tor Browser Bundle binary packages from source. This represents a major improvement in the security of the Tor software build and distribution processes against targeted attacks.  The motivations and technical details of this work will appear in future Tor Project blog posts.</p>

<h1>Minor progress on datagram-based transport</h1>

<p><a href="https://blog.torproject.org/blog/moving-tor-datagram-transport" rel="nofollow">As Steven Murdoch explained in 2011</a>, in the current implementation of Tor, “when a packet gets dropped or corrupted on a link between two Tor nodes, […], all circuits passing through this pair of nodes will be stalled, not only the circuit corresponding to the packet which was dropped.” This is because traffic from multiple circuits heading into an OR node are multiplexed by default into a single TCP connection. However, when the reliability and congestion control requirements of TCP streams are enforced (by the operating system) on this multiplexed connection, a situation is created in which one poor quality circuit can disproportionately slow down the others.</p>

<p>This shortcoming could be worked around by migrating Tor from TCP to a datagram-based transport protocol. Nick Mathewson opened <a href="https://bugs.torproject.org/9165" rel="nofollow">#9165</a> to track progress on the matter.</p>

<p>Late last year, Steven Murdoch began an <a href="https://gitweb.torproject.org/sjm217/tor.git/shortlog/refs/heads/utp" rel="nofollow">experimental Tor branch using uTP</a>, a protocol “which provides reliable, ordered delivery while maintaining minimum extra delay”, and is already <a href="http://www.bittorrent.org/beps/bep_0029.html" rel="nofollow">used by uTorrent for peer-to-peer connections</a>. Nick Mathewson finally got to review his work and wrote several comments on <a href="https://bugs.torproject.org/9166" rel="nofollow">#9166</a>. The code isn’t close to production-quality right now; it is just good enough for performance testing.</p>

<h1>obfsproxyssh</h1>

<p>Yawning Angel sent out <a href="https://lists.torproject.org/pipermail/tor-dev/2013-June/005083.html" rel="nofollow">a request for comments</a> on the very first release of <a href="https://github.com/Yawning/obfsproxyssh" rel="nofollow">obfsproxyssh</a>, a pluggable transport that uses the ssh wire protocol to hide Tor traffic. Its behavior would appear to potential eavesdroppers to be “identical to a user sshing to a host, authenticating with a RSA public/private key pair and opening a direct-tcp channel to the ORPort of the bridge.”</p>

<p>The announcement contains several open issues and questions. Feel free to have a look and voice your comments!</p>

<h1>Crowdfunding for Tor exit relays and bridges</h1>

<p>Moritz Bartl announced that he has started a <a href="http://www.indiegogo.com/projects/tor-anti-censorship-and-anonymity-infrastructure/" rel="nofollow">crowdfunding campaign for Tor exit relays and bridges</a>.</p>

<p>The donations will be distributed equally among all Torservers.net partner organizations (Zwiebelfreunde e.V., DFRI, Nos Oignons, Swiss Privacy Foundation, Frënn vun der Ënn and NoiseTor).</p>

<p>For a faster and better network, chip in and spread the word!</p>

<h1>Tails 0.19 is out, new stable Tor Browser Bundles</h1>

<p>On Wednesday, June 26, two of the most popular Tor projects both made new releases: the Tor Browser Bundle, and Tails, The Amnesiac Incognito Live System. Users are encouraged to upgrade as soon as possible.</p>

<p>The <a href="https://blog.torproject.org/blog/new-tor-browser-bundles-and-tor-02414-alpha-packages" rel="nofollow">stable Tor Browser Bundle was updated to version 2.3.25-10</a>, and includes fixes from upstream Firefox 17.0.7esr. <a href="https://tails.boum.org/news/version_0.19/" rel="nofollow">Tails 0.19</a> includes the new stable Tor Browser, along with an updated 3.9.5 kernel and minor security improvements to wireless, GNOME and GnuPG defaults.</p>

<h1>Jenkins + Stem catching their first regression</h1>

<p>Quoting <a href="https://lists.torproject.org/pipermail/tor-reports/2013-June/000262.html" rel="nofollow">Damian Johnson’s June status report</a>: “Our automated<br />
Jenkins test runs caught their first instance of tor regression. This<br />
concerned LOADCONF’s behavior after merging a branch for ticket #6752”.<br />
A <a href="https://bugs.torproject.org/9122" rel="nofollow">new ticket</a> was opened after Damian properly identified the issue.</p>

<h1>First round of reports from GSoC projects</h1>

<p><a href="https://lists.torproject.org/pipermail/tor-dev/2013-June/005078.html" rel="nofollow">Johannes Fürmann reported</a> on his project, a virtual network environment intended to simulate censorship for OONI (dubbed “Evil Genius”, after Descartes). <a href="https://lists.torproject.org/pipermail/tor-dev/2013-June/005082.html" rel="nofollow">Hareesan reported</a> on the steganography browser addon. <a href="https://lists.torproject.org/pipermail/tor-dev/2013-June/005085.html" rel="nofollow">Cristian-Matei Toader is working</a> on adding capabilities-based sandboxing to Tor on Linux, using the kernel’s seccomp syscall filtering mechanism. <a href="https://lists.torproject.org/pipermail/tor-dev/2013-June/005086.html" rel="nofollow">Chang Lan implemented</a> a HTTP proxy-based transport using CONNECT as the first step in his efforts to implement a general Tor-over-HTTP pluggable transport.</p>

<h1>Monthly status reports for June 2013</h1>

<p>The wave of regular monthly reports from Tor project members for the month of June has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2013-June/000262.html" rel="nofollow">Damian Johnson</a>’s was the first, followed soon after by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2013-June/000263.html" rel="nofollow">Philipp Winter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000264.html" rel="nofollow">Colin C.</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000266.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000267.html" rel="nofollow">Lunar</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000268.html" rel="nofollow">Moritz Bartl</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000269.html" rel="nofollow">Jason Tsai</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000270.html" rel="nofollow">Andrew Lewman</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000271.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000272.html" rel="nofollow">Kelley Misata</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000273.html" rel="nofollow">Matt Pagan</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000276.html" rel="nofollow">Andrea Shepard</a>.</p>

<h1>Tor on StackExchange</h1>

<p>The <a href="http://area51.stackexchange.com/proposals/56447/tor-online-anonymity-privacy-and-security" rel="nofollow">proposed StackExchange Q&amp;A page for Tor</a> has left the “initial definition” stage and has entered the “commitment” stage on Area 51. During <a href="https://lists.torproject.org/pipermail/tor-talk/2013-June/028473.html" rel="nofollow">this stage</a>, interested users are asked to digitally “sign” the proposal with their name to help ensure the site will have an active community during its critical early days.</p>

<h1>Forensic analysis of the Tor Browser Bundle</h1>

<p>On Friday, June 28, Runa Sandvik published Tor Tech Report 2013-06-001, titled <a href="https://research.torproject.org/techreports/tbb-forensic-analysis-2013-06-28.pdf" rel="nofollow">Forensic Analysis of the Tor Browser Bundle on OS X, Linux, and Windows</a>, as part of a deliverable project for two Tor sponsors. The report is a detailed write-up of the forensic experiments Sandvik has been <a href="http://encrypted.cc/post/51552592311/forensic-analysis-of-tor-on-os-x" rel="nofollow">documenting on her blog</a>, the goal of which was “to identify traces left behind by the Tor Browser Bundle after extracting, using, and deleting the bundle”.</p>

<p>In short, each platform indeed retains forensic traces of the existence of the Tor Browser Bundle. Many “are related to default operating system settings, some of which the bundle might not be able to remove. We therefore <a href="https://bugs.torproject.org/7033" rel="nofollow">propose the creation of a document</a> which lists steps our users can take to mitigate these traces on the different operating systems.”</p>

<p>Of course, Tor Browser Bundle users wishing to take immediate action to prevent the creation of forensic traces are not out of luck: “the easiest way to avoid leaving traces on a computer system is to use <a href="https://tails.boum.org/" rel="nofollow">The Amnesiac Incognito Live System (Tails)</a>.”</p>

<h1>Miscellaneous development news</h1>

<p>David Goulet is <a href="https://lists.torproject.org/pipermail/tor-dev/2013-June/005069.html" rel="nofollow">making good progress</a> on his <a href="https://lists.torproject.org/pipermail/tor-dev/2013-June/004959.html" rel="nofollow">rewrite of torsocks</a> and should have a beta ready in a couple of weeks. He awaits your code reviews, comments and contributions.</p>

<p>Leo Unglaub ran into some trouble with a dependency just as he was about to publish the <a href="https://lists.torproject.org/pipermail/tor-dev/2013-June/005084.html" rel="nofollow">work-in-progress code for his Vidalia replacement</a>.</p>

<p>Nick Mathewson did some analysis on <a href="https://bugs.torproject.org/7009" rel="nofollow">possible methods for reducing the volume of fetched directory information</a>, by running some scripts over the last month of consensus directories.</p>

<h1>A vulnerability affecting microdescriptors in Tor?</h1>

<p>On Friday, June 28 an anonymous individual <a href="https://twitter.com/ewrwerwtretetet/status/350815079882686464" rel="nofollow">contacted Tor developers</a> over Twitter <a href="http://pastebin.com/pRiMx0CW" rel="nofollow">claiming to have found a vulnerability</a> in the way microdescriptors are validated by Tor clients which would allow “determination of the source and end-point of a given [victim’s] tor connection with little more than a couple relays and some rogue directory authorities [both controlled by the adversary].”</p>

<p><a href="https://lists.torproject.org/pipermail/tor-talk/2013-June/028699.html" rel="nofollow">Detailed</a> <a href="https://lists.torproject.org/pipermail/tor-talk/2013-June/028700.html" rel="nofollow">testing</a> by Nick Mathewson could not reproduce the behavior in the Tor client that was claimed to enable such an attack. After a lengthy Twitter debate with Mathewson, the reporter disappeared, no bugs have been filed, and it appears the vulnerability was nothing of the sort.  Without being able to verify the existence of the claimed vulnerability, Mathewson <a href="https://lists.torproject.org/pipermail/tor-talk/2013-June/028701.html" rel="nofollow">concluded</a> that the reporter’s described attack was equivalent “at worst… to the ‘request filtering’ attack… which has defenses”.</p>

<p>The issue was also <a href="http://seclists.org/fulldisclosure/2013/Jun/245" rel="nofollow">mentioned</a> (and likewise dismissed) on the security mailing list, Full Disclosure.</p>

<p>For anyone interested in reporting vulnerabilities in Tor software, please avoid following that example. Until a process gets <a href="https://bugs.torproject.org/9186" rel="nofollow">documented</a>, the best way to report the discovery of a vulnerability is to get in touch with one of the Tor core developers using encrypted email.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, dope457, moskvax, Mike Perry, Nick Mathewson, mttp, and luttigdev.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteer writers who watch the Tor community and report about what is going on. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a> and write down your name if you want to get involved!</p>


title: Tor Weekly News — March 5th, 2014
---
pub_date: 2014-03-05
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the ninth issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Tor 0.2.4.21 is out</h1>

<p>Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-talk/2014-March/032242.html" rel="nofollow">announced the release of Tor 0.2.4.21</a>, whose major new feature is the forced inclusion of at least one NTor-capable relay in any given three-hop circuit as a defence against adversaries who might be able to break 1024-bit encryption; this feature was first seen in the <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032150.html" rel="nofollow">latest alpha release</a> (0.2.5.2-alpha) three weeks ago, but is here incorporated into the current stable series.</p>

<p>You can find full details of this release’s other features and bugfixes in Roger’s announcement.</p>

<h1>Tor in Google Summer of Code 2014</h1>

<p>As has been the case over the past several years, Tor will once again <a href="https://www.google-melange.com/gsoc/org2/google/gsoc2014/tor" rel="nofollow">be participating</a> in Google’s annual Summer of Code program — aspiring software developers have the chance to work on a Tor-related project with financial assistance from Google and expert guidance from a core Tor Project member. Several prospective students have already contacted the community with questions about the program, and Damian Johnson took to the Tor Blog to give a <a href="https://blog.torproject.org/blog/tor-google-summer-code-2014" rel="nofollow">brief summary of what students can expect from the Summer of Code</a>, and what the Tor Project expects from its students.</p>

<p>In particular, Damian encouraged potential applicants to discuss their ideas with the community on the tor-dev mailing list or IRC channel before submitting an application: “Communication is essential to success in the summer of code, and we’re unlikely to accept students we haven’t heard from before reading their application.”</p>

<p>If you are hoping to contribute to Tor as part of the Summer of Code program, please have a look through Damian’s advice and then, as he says, “come to the list or IRC channel and talk to us!”</p>

<h1>Two ways to help with Tails development</h1>

<p>One of the most interesting upcoming additions to the Tails operating system is the ability to thwart attempts at tracking the movements of network-enabled devices by spoofing the MAC address on each boot.  As part of the testing process for this new feature, the Tails developers have <a href="https://tails.boum.org/news/spoof-mac/" rel="nofollow">released</a> an experimental disk image which turns it on by default, alongside a step-by-step guide to trying it out and reporting any issues encountered. However, as the developers state, “this is a test image. Do not use it for anything other than testing this feature.” If you are willing to take note of this caveat, please feel free to download the test image and let the community know what you find.</p>

<p>Turning to the longer-term development of the project, the team also published a detailed set of guidelines for anyone who wants to help <a href="https://tails.boum.org/contribute/how/debian/" rel="nofollow">improve Tails itself by contributing to the development of Debian</a>, the operating system on which Tails is based. They include advice on the relationship between the two distributions, tasks in need of attention, and channels for discussing issues with the Tails community; if you are keen on the idea of helping two free-software projects at one stroke, please have a look!</p>

<h1>Monthly status reports for February 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of February has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000464.html" rel="nofollow">Georg Koppen</a> released his report first, followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000465.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000466.html" rel="nofollow">Pearl Crescent</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000467.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000468.html" rel="nofollow">Colin C.</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000471.html" rel="nofollow">Lunar</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000472.html" rel="nofollow">Kelley Misata</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000474.html" rel="nofollow">Damian Johnson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000475.html" rel="nofollow">George Kadianakis</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000476.html" rel="nofollow">Philipp Winter</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000478.html" rel="nofollow">Karsten Loesing</a>.</p>

<p>Lunar also reported on behalf of <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000469.html" rel="nofollow">the help desk</a>, while Mike Perry did the same on behalf of <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000473.html" rel="nofollow">the Tor Browser team </a>, and Arturo Filastò for <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000477.html" rel="nofollow">the OONI team</a>.</p>

<h1>Miscellaneous news</h1>

<p>Members of the Prosecco research team released a <a href="https://secure-resumption.com/" rel="nofollow">new attack on the TLS protocol</a> — dubbed “Triple Handshake” — allowing impersonation of a given client when client authentication is in use together with session resumption and renegotiation. Nick Mathewson published a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-March/006372.html" rel="nofollow">detailed analysis of why Tor is not affected</a>, and also outlines future changes to make Tor resistant to even more potential TLS issues.</p>

<p>Mike Perry <a href="https://lists.torproject.org/pipermail/tbb-dev/2014-February/000000.html" rel="nofollow">announced</a> the start of a weekly Tor Browser developer’s meeting, to be held on <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #666666; font-style: italic;">#tor-dev</span></code></span> on <span class="geshifilter"><code class="php geshifilter-php">irc<span style="color: #339933;">.</span>oftc<span style="color: #339933;">.</span>net</code></span>. These meetings are tentatively scheduled for 19:00 UTC on Wednesdays. Details on the format and flow of the meetings can be found on the tor-dev and <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tbb-dev" rel="nofollow">tbb-dev</a> mailing lists.</p>

<p>Roger Dingledine and Nick Mathewson were among the signatories of an <a href="https://www.eff.org/deeplinks/2014/02/open-letter-to-tech-companies" rel="nofollow">open letter</a> published by the EFF which offers ten principles for technology companies to follow in protecting users from illegal surveillance.</p>

<p>Nick Mathewson also <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006341.html" rel="nofollow">detailed</a> a change in the way that the core Tor development team will use the bugtracker’s “milestone” feature to separate tickets marked for resolution in a given Tor version from those that can be deferred to a later release.</p>

<p>Nick then sent out the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006342.html" rel="nofollow">latest in his irregular series of Tor proposal status updates</a>, containing summaries of each open proposal, guidance for reviewers, and notes for further work. If you'd like to help Tor’s development by working on one of these proposals, start here!</p>

<p>On the subject of proposals, two new ones were sent to the tor-dev list for review: <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006304.html" rel="nofollow">proposal 228</a>, which offers a way for relays to prove ownership of their onion keys as well as their identity key, and <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006340.html" rel="nofollow">proposal 229</a> based on Yawning Angel’s unnumbered submission from last week, which concerns improvements to the SOCKS5 protocol for communication between clients, Tor, and pluggable transports.</p>

<p>Nicholas Merrill <a href="https://mailman.stanford.edu/pipermail/liberationtech/2014-February/013041.html" rel="nofollow">wrote</a> to the Liberationtech list to announce that xmpp.net now lists <a href="https://xmpp.net/reports.php#onions" rel="nofollow">XMPP servers that are reachable over hidden services</a>, and that xmpp.net’s server scanner works with these as well.</p>

<p>Patrick Schleizer <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032227.html" rel="nofollow">announced</a> the release of version 8 of <a href="https://www.whonix.org/" rel="nofollow">Whonix</a> — an operating system focused on anonymity, privacy and security based on the Tor anonymity network, Debian and security by isolation. The curious should take a look at the long changelog.</p>

<p>Kelley Misata wrote up an <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000470.html" rel="nofollow">account</a> of her talk “Journalists — Staying Safe in a Digital World”, which she delivered at the Computer-Assisted Reporting Conference in Baltimore.</p>

<p>Having co-authored <a href="http://petsymposium.org/2012/papers/hotpets12-1-usability.pdf" rel="nofollow">a paper in 2012 on usability issues connected with<br />
the Tor Browser Bundle</a>, Greg Norcie <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032205.html" rel="nofollow">drew attention</a> to a follow-up study named <a href="http://www.norcie.com/papers/torUSEC.pdf" rel="nofollow">Why Johnny Can’t Blow the Whistle</a>,  which focuses on verifying the conclusions of the earlier tests while exploring a number of other possible usability improvements. The study was, however, carried out before the release of Tor Browser version 3, which improved the bundle’s usability based on earlier suggestions.</p>

<p>Mac OS X users will be thrilled to learn that the next Tor Browser Bundle will be <a href="https://people.torproject.org/~mikeperry/images/TBBDMG.png" rel="nofollow">shipped as a DMG (disk image)</a> instead of the <a href="https://bugs.torproject.org/4261" rel="nofollow">previous unusual .ZIP archive</a>.</p>

<p>David Rajchenbach-Teller from Mozilla <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032204.html" rel="nofollow">reached out</a> to the Tor Browser developers about their overhaul of the Firefox Session Restore mechanism. This is another milestone in the growing collaboration between the Tor Project and Mozilla.</p>

<p>On the “anonymity is hard” front, David Fifield <a href="https://bugs.torproject.org/10703" rel="nofollow">reported</a> a fingerprinting issue on the Tor Browser. Fallback charsets can be used to learn the user locale as they vary from one to another. The next release of the Tor Browser will use “windows-1252” for all locales, as this matches the impersonated “User-Agent” string (Firefox — English version — on Windows) that it already sends in its HTTP headers.</p>

<p>Yawning Angel <a href="https://lists.torproject.org/pipermail/tor-dev/2014-March/006358.html" rel="nofollow">called for help</a> in testing and reviewing obfsclient-0.0.1rc2, the second obfsclient release candidate this week: “assuming nothing is broken, this will most likely become v0.0.1, though I may end up disabling Session Ticket handshakes.”</p>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2014-March/006356.html" rel="nofollow">published</a> a guide to patching meek, an HTTP pluggable transport, so that it can be used to send traffic via <a href="https://www.getlantern.org/" rel="nofollow">Lantern</a>, a censorship circumvention system which “acts as an HTTP proxy and proxies your traffic through trusted friends.”</p>

<p>Fortasse <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032220.html" rel="nofollow">started a discussion</a> on tor-talk about using HTTPS Everywhere to redirect Tor Browser users to .onion addresses when available. Several people commented regarding the procedure, its security, or how it could turn the Tor Project or the EFF into some kind of registrar.</p>

<p>anonym <a href="https://mailman.boum.org/pipermail/tails-dev/2014-February/005023.html" rel="nofollow">has been busy</a> adapting the configuration interface from the Tor Browser — called “Tor Launcher” — to Tails’ needs. Preliminary results can already be seen in the <a href="http://nightly.tails.boum.org/build_Tails_ISO_experimental/" rel="nofollow">images built from the experimental branch</a>.</p>

<p>Ramo <a href="https://lists.torproject.org/pipermail/tor-relays/2014-March/004007.html" rel="nofollow">wrote</a> to announce their <a href="https://github.com/goodvikings/tor_nagios/" rel="nofollow">Nagios plugin project</a> to the relay operator community. Lunar pointed out a complementary probe named<br />
<a href="http://anonscm.debian.org/gitweb/?p=users/lunar/check_tor.git;a=blob;f=README;hb=HEAD" rel="nofollow">check_tor.py</a>.</p>

<p>Virgil Griffith sent a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006344.html" rel="nofollow">draft proposal</a> for changes to improve the latency of hidden services when using the “Tor2web” mode. Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-dev/2014-March/006347.html" rel="nofollow">commented</a> that one of the proposed changes actually opened a new research question regarding the actual latency benefits.</p>

<p>David Goulet released the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-March/006371.html" rel="nofollow">fourth candidate of his Torsocks rewrite</a>. This new version comes after “a big code review from Nick and help from a lot of people contributing and testing”. But more reviews and testing are now welcome!</p>

<h1>Tor help desk roundup</h1>

<p>Often users email the help desk when the Tor Browser’s Tor client fails somehow. There are many ways for the Tor Browser to fail in such a way that the Tor log is inaccessible. Since antivirus programs, firewalls, system clock skew, proxied internet connections, and internet censorship have all been known to cause Tor failures, it is not always easy to determine the source of the problem.  Thankfully, the Tor Browser team is working on making the logs easier to access in case of failures (<a href="https://trac.torproject.org/projects/tor/ticket/10059" rel="nofollow">#10059</a>, <a href="https://trac.torproject.org/projects/tor/ticket/10603" rel="nofollow">#10603</a>).</p>

<h1>News from Tor StackExchange</h1>

<p>Janice needs to be able to connect from an IP address in a specific city and wanted to know if <a href="https://tor.stackexchange.com/q/1485/88" rel="nofollow">Tor can be used to do so</a>. Several users suggested that this is not possible with Tor. For city-level IP addresses, it might better to use other services like a proxy or a tunnel, provided one does not require anonymity.</p>

<p>The Tor Browser Bundle sets the default font to Times New Roman 16pt and allows pages to use their own fonts. User joeb likes to change the settings and wondered <a href="https://tor.stackexchange.com/q/1619/88" rel="nofollow">how this increases the possibility to fingerprint a user</a>. gacar suggested that this will facilitate fingerprinting attacks. Several important sites use <a href="https://www.cosic.esat.kuleuven.be/fpdetective/#results" rel="nofollow">font probing to fingerprint their users</a>, and changing the default fonts is likely to make a user stand out from the common anonymity set.</p>

<p>Kristopher Ives <a href="https://tor.stackexchange.com/q/1598/88" rel="nofollow">wondered</a> if Tor uses some kind of compression. Several users <a href="https://gitweb.torproject.org/tor.git?a=search&amp;h=HEAD&amp;st=grep&amp;s=gzip" rel="nofollow">searched the source code archives for “gzip”</a> and found code which deals with directory information. Jens Kubieziel argued that Tor operates on encrypted data and compressing encrypted data usually results in a increase in size, so it makes no sense to compress this data.</p>

<p>Stackexchange uses bounties to award higher reputations to answers. By using this one can attract attention and get better answers or an answer at all. The question about <a href="https://tor.stackexchange.com/q/1503/88" rel="nofollow">using DNSSEC and DNScrypt over Tor</a> is probably the first to receive a bounty: an answer to this question would be rewarded with 50 points. However, they have not been earned yet, so if you know an answer, please enlighten the rest of the community.</p>

<p>This issue of Tor Weekly News has been assembled by harmony, Lunar, qbi, Matt Pagan, Karsten Loesing, Mike Perry, dope457, and Philipp Winter.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>


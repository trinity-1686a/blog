title: Tor Browser 7.5a6 is released
---
pub_date: 2017-10-20
---
author: boklm
---
tags:

tor browser
tbb
tbb-7.5
---
categories: applications
---
_html_body:

<p>Tor Browser 7.5a6 is now available from the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha">Tor Browser Project page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/7.5a6/">distribution directory</a>.</p>
<p>This release updates firefox to <a href="https://www.mozilla.org/en-US/firefox/52.4.1/releasenotes/">52.4.1esr</a>, Tor to <a href="https://blog.torproject.org/tor-0322-alpha-released">0.3.2.2-alpha</a>, HTTPS-Everywhere to 2017.10.4 and NoScript to 5.1.2. This release is also fixing some crashes and adding a donation banner starting on Oct 23 in order to point to our end-of-the-year 2017 donation campaign.</p>
<p>The full changelog since Tor Browser 7.5a5 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 52.4.1esr</li>
<li>Update Tor to 0.3.2.2-alpha</li>
<li>Update Torbutton to 1.9.8.2
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23887">Bug 23887</a>: Update banner locales and Mozilla text</li>
<li>Translations update</li>
</ul>
</li>
<li>Update HTTPS-Everywhere to 2017.10.4</li>
<li>Update NoScript to 5.1.2
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23723">Bug 23723</a>: Loading entities from NoScript .dtd files is blocked</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23724">Bug 23724</a>: NoScript update breaks Security Slider and its icon disappears</li>
</ul>
</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23745">Bug 23745</a>: Tab crashes when using Tor Browser to access Google Drive</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23694">Bug 23694</a>: Update the detailsURL in update responses</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22501">Bug 22501</a>: Requests via javascript: violate FPI</li>
</ul>
</li>
<li>OS X
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23807">Bug 23807</a>: Tab crashes when playing video on High Sierra</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23025">Bug 23025</a>: Add some hardening flags to macOS build</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-271962"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271962" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>billy goat gruff (not verified)</span> said:</p>
      <p class="date-time">October 20, 2017</p>
    </div>
    <a href="#comment-271962">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271962" class="permalink" rel="bookmark">thank you so much again for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thank you so much again for all your free hard work.  we owe you a lot!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272032"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272032" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>JackD (not verified)</span> said:</p>
      <p class="date-time">October 23, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271962" class="permalink" rel="bookmark">thank you so much again for…</a> by <span>billy goat gruff (not verified)</span></p>
    <a href="#comment-272032">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272032" class="permalink" rel="bookmark">&quot;thank you so much again for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"thank you so much again for all your free hard work. we owe you a lot!"<br />
- Hear-hear!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-271969"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271969" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2017</p>
    </div>
    <a href="#comment-271969">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271969" class="permalink" rel="bookmark">There&#039;s some bug in this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There's some bug in this version of the tor client that drives me nuts, even when I'm connected to my network at some point I get:</p>
<p><div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #009900;">&#91;</span>notice<span style="color: #009900;">&#93;</span> Tor has not observed any network activity <span style="color: #b1b100;">for</span> the past <span style="color: #cc66cc;">80</span> seconds<span style="color: #339933;">.</span> Disabling circuit build timeout recording<span style="color: #339933;">.&lt;/</span>p<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>p<span style="color: #339933;">&gt;</span><span style="color: #009900;">&#91;</span>warn<span style="color: #009900;">&#93;</span> Failed to find node <span style="color: #b1b100;">for</span> hop <span style="color: #666666; font-style: italic;">#1 of our path. Discarding this circuit.&lt;br /&gt;</span></div></li></ol></pre></div></p>
<p>And then I have to restart the browser so that Tor bootstraps again and starts working again. This happened with Tor 0.3.2.1 but it intensified a lot with the recent version (I'm using Tor with bridges FWIW) Hope this bug gets higher priority.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271970"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271970" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">October 20, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271969" class="permalink" rel="bookmark">There&#039;s some bug in this…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-271970">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271970" class="permalink" rel="bookmark">Is this a problem you only…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is this a problem you only have seen in the alpha, or does it also affect Tor Browser 7.0.7?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271971"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271971" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-271971">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271971" class="permalink" rel="bookmark">Only alpha with Tor 0.3.2.x …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Only alpha with Tor 0.3.2.x (i.e. these last two releases of TBBa)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271973"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271973" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">October 20, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271971" class="permalink" rel="bookmark">Only alpha with Tor 0.3.2.x …</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-271973">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271973" class="permalink" rel="bookmark">I created a ticket for this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I created a ticket for this issue:<br />
<a href="https://trac.torproject.org/projects/tor/ticket/23923" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/23923</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-271984"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271984" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>1stHOP The Same (not verified)</span> said:</p>
      <p class="date-time">October 20, 2017</p>
    </div>
    <a href="#comment-271984">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271984" class="permalink" rel="bookmark">Admin, my 1st hop is the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Admin, my 1st hop is the same (Canada) everytime i use TOR. In the past there was a map of the hops and they were different everytime.  The only way for me to change the first hop is to uninstall TOR and then reinstall.   There has to be an easier way,</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271998"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271998" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">October 21, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271984" class="permalink" rel="bookmark">Admin, my 1st hop is the…</a> by <span>1stHOP The Same (not verified)</span></p>
    <a href="#comment-271998">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271998" class="permalink" rel="bookmark">https://trac.torproject.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://trac.torproject.org/projects/tor/wiki/org/teams/CommunityTeam/Support#WhyisthefirstIPaddressinmyrelaycircuitalwaysthesame" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/org/teams/CommunityTeam/S…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-271985"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271985" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>luweitest (not verified)</span> said:</p>
      <p class="date-time">October 20, 2017</p>
    </div>
    <a href="#comment-271985">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271985" class="permalink" rel="bookmark">Are there some document that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are there some document that could teach me to setup tor and meek for my existing Firefox? I downloaded TOR expert bundle and extracted meek from tor-browser but don't know how to chain them up.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271991"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271991" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ano (not verified)</span> said:</p>
      <p class="date-time">October 21, 2017</p>
    </div>
    <a href="#comment-271991">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271991" class="permalink" rel="bookmark">I&#039;m currently using tails…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm currently using tails and I'm wondering if it's possible to install the experimental tor browser while keeping the stable tor browser?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272045"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272045" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">October 23, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271991" class="permalink" rel="bookmark">I&#039;m currently using tails…</a> by <span>ano (not verified)</span></p>
    <a href="#comment-272045">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272045" class="permalink" rel="bookmark">You can download both…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can download both versions and keep them in different folders, but you can only have 1 open at a time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272002"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272002" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>SUPPORT THE TOR (not verified)</span> said:</p>
      <p class="date-time">October 21, 2017</p>
    </div>
    <a href="#comment-272002">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272002" class="permalink" rel="bookmark">All TOR users , please…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>All TOR users , please donate to keep things going!</p>
<p>TOR FAN</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272028"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272028" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jeks webs (not verified)</span> said:</p>
      <p class="date-time">October 23, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272002" class="permalink" rel="bookmark">All TOR users , please…</a> by <span>SUPPORT THE TOR (not verified)</span></p>
    <a href="#comment-272028">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272028" class="permalink" rel="bookmark">I want visit tor, it&#039;s my…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I want visit tor, it's my wish. I have not enough mony plz help to visit</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272022"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272022" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Momplaisir  (not verified)</span> said:</p>
      <p class="date-time">October 22, 2017</p>
    </div>
    <a href="#comment-272022">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272022" class="permalink" rel="bookmark">pretty good app</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>pretty good app</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272031"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272031" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jackd (not verified)</span> said:</p>
      <p class="date-time">October 23, 2017</p>
    </div>
    <a href="#comment-272031">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272031" class="permalink" rel="bookmark">torbrowser-install-7.5a6_en…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>torbrowser-install-7.5a6_en-US.exe gets multiple malware hits on virustotal.com<br />
eGambit - malicious_confidence_74%<br />
Rising - Malware.Heuristic!ET#100%<br />
any need for concern?  thx</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272055"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272055" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2017</p>
    </div>
    <a href="#comment-272055">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272055" class="permalink" rel="bookmark">I&#039;d appreciate it a lot if…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'd appreciate it a lot if anyone can enlighten me on whether my fingerprint is OK <a href="https://browserprint.info/view?source1=UUID&amp;UUID1UUID=209a5846-faea-4346-9059-b2fc70e94fd2" rel="nofollow">https://browserprint.info/view?source1=UUID&amp;UUID1UUID=209a5846-faea-434…</a> (Using latest TB alpha on Linux)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272056"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272056" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Bastian (not verified)</span> said:</p>
      <p class="date-time">October 24, 2017</p>
    </div>
    <a href="#comment-272056">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272056" class="permalink" rel="bookmark">Thanks for the good work</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the good work</p>
</div>
  </div>
</article>
<!-- Comment END -->

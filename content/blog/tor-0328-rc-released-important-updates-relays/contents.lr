title: Tor 0.3.2.8-rc is released, with important updates for relays
---
pub_date: 2017-12-21
---
author: nickm
---
_html_body:

<p>Tor 0.3.2.8-rc fixes a pair of bugs in the KIST and KISTLite schedulers that had led servers under heavy load to overload their outgoing connections. All relay operators running earlier 0.3.2.x versions should upgrade. This version also includes a mitigation for over-full DESTROY queues leading to out-of-memory conditions: if it works, we will soon backport it to earlier release series.</p>
<p>This is the second release candidate in the 0.3.2 series. If we find no new bugs or regression here, then the first stable 0.3.2 release will be nearly identical to this.</p>
<p>You can download the source from the usual place on the website. Binary packages should be available soon. There probably won't be a Tor Browser release for this one; this issues fixed here are mainly (but not exclusively) relevant to relays.<br />
 </p>
<h2>Changes in version 0.3.2.8-rc - 2017-12-21</h2>
<ul>
<li>Major bugfixes (KIST, scheduler):
<ul>
<li>The KIST scheduler did not correctly account for data already enqueued in each connection's send socket buffer, particularly in cases when the TCP/IP congestion window was reduced between scheduler calls. This situation lead to excessive per-connection buffering in the kernel, and a potential memory DoS. Fixes bug <a href="https://bugs.torproject.org/24665">24665</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the December 6 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (hidden service v3):
<ul>
<li>Bump hsdir_spread_store parameter from 3 to 4 in order to increase the probability of reaching a service for a client missing microdescriptors. Fixes bug <a href="https://bugs.torproject.org/24425">24425</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (memory usage):
<ul>
<li>When queuing DESTROY cells on a channel, only queue the circuit-id and reason fields: not the entire 514-byte cell. This fix should help mitigate any bugs or attacks that fill up these queues, and free more RAM for other uses. Fixes bug <a href="https://bugs.torproject.org/24666">24666</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (scheduler, KIST):
<ul>
<li>Use a sane write limit for KISTLite when writing onto a connection buffer instead of using INT_MAX and shoving as much as it can. Because the OOM handler cleans up circuit queues, we are better off at keeping them in that queue instead of the connection's buffer. Fixes bug <a href="https://bugs.torproject.org/24671">24671</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-273185"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273185" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2017</p>
    </div>
    <a href="#comment-273185">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273185" class="permalink" rel="bookmark">Thank you for the latest…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for the latest update 21 Dec 2017</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273208"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273208" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>No (not verified)</span> said:</p>
      <p class="date-time">December 24, 2017</p>
    </div>
    <a href="#comment-273208">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273208" class="permalink" rel="bookmark">I can&#039;t extract the tar…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't extract the tar archive, is it a problem on my side or can you fix it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273236"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273236" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2017</p>
    </div>
    <a href="#comment-273236">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273236" class="permalink" rel="bookmark">What is the status of option…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is the status of option <span class="geshifilter"><code class="php geshifilter-php">__AllDirActionsPrivate</code></span> in current tor versions? It is not documented in <span class="geshifilter"><code class="php geshifilter-php">man torrc</code></span>, but I can see some mentioning in <a href="https://gitweb.torproject.org/torspec.git/tree/control-spec.txt" rel="nofollow">control-spec.txt</a>, in <a href="https://trac.torproject.org/projects/tor/ticket/21576#comment:6" rel="nofollow">bug reports</a> (with relation to crash, probably already fixed), and in old (more than 10 years) Roger's <a href="http://tor-talk.torproject.narkive.com/9TijtTEH/blocked-by-websense" rel="nofollow">recommendations</a>. Is it safe to use?</p>
<p>I need my tor parsing programs to use full nodes' descriptors. <a href="https://nyx.torproject.org/#missing_relay_details" rel="nofollow">Recommended way</a> to get it is to enable options</p>
<p><span class="geshifilter"><code class="php geshifilter-php">FetchDirInfoEarly <span style="color: #cc66cc;">1</span></code></span><br />
<span class="geshifilter"><code class="php geshifilter-php">FetchDirInfoExtraEarly <span style="color: #cc66cc;">1</span></code></span><br />
<span class="geshifilter"><code class="php geshifilter-php">FetchUselessDescriptors <span style="color: #cc66cc;">1</span></code></span></p>
<p>However, it results in like 10 extra connections of my tor client to some random tor nodes. I guess this can be used to easily profile me among other tor clients. So, I think about possibility to start my tor client with standard config and then, after start, do <span class="geshifilter"><code class="php geshifilter-php">SETCONF</code></span> through <span class="geshifilter"><code class="php geshifilter-php">ControlPort</code></span> (with these 3 options and with the option <span class="geshifilter"><code class="php geshifilter-php">__AllDirActionsPrivate</code></span>). I suppose that from outside it will look like more typical tor client connection than without the option <span class="geshifilter"><code class="php geshifilter-php">__AllDirActionsPrivate</code></span>. What's Tor Project's opinion on this solution?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273272"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273272" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Peter Ott (not verified)</span> said:</p>
      <p class="date-time">January 02, 2018</p>
    </div>
    <a href="#comment-273272">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273272" class="permalink" rel="bookmark">Hello,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,<br />
any idea (link) from where to download the 0.3.2.8-rc for Windows. TOR gives a warning that the 0.3.2-rc is outdated.<br />
I'm running only a relay so the 'Expert bundle' is needed not the 'Browser'. Thanks in advance.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273384"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273384" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">January 12, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273272" class="permalink" rel="bookmark">Hello,…</a> by <span>Peter Ott (not verified)</span></p>
    <a href="#comment-273384">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273384" class="permalink" rel="bookmark">There is non so far. We have…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is non so far. We have 0.3.2.7-rc available in the Tor Browser 7.5a10 directory, see: <a href="https://archive.torproject.org/tor-package-archive/torbrowser/7.5a10/" rel="nofollow">https://archive.torproject.org/tor-package-archive/torbrowser/7.5a10/</a> (the .zip files). The next Tor Browser release (due on Jan 23) will have the latest Tor stable version available.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273302"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273302" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Amin amin (not verified)</span> said:</p>
      <p class="date-time">January 04, 2018</p>
    </div>
    <a href="#comment-273302">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273302" class="permalink" rel="bookmark">Nice</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nice</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273383"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273383" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Red (not verified)</span> said:</p>
      <p class="date-time">January 11, 2018</p>
    </div>
    <a href="#comment-273383">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273383" class="permalink" rel="bookmark">A question what are you guys…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A question what are you guys opinion of googles search lock add on to chrome. I am just starting to learn what you guys already know. Recently retired and have found i like to learn about computing. Not that I know much but google is pushing me toward search lock. I am not sure I like being pushed.It all started with me disabling there info gathering from my google account.Is search lock a benefit? whats in it for google? Whats the best road for an inexperienced but growing user. Thank you Red</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273524"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273524" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jose (not verified)</span> said:</p>
      <p class="date-time">January 23, 2018</p>
    </div>
    <a href="#comment-273524">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273524" class="permalink" rel="bookmark">1/23/2018 22:39:06 PM.300 …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>1/23/2018 22:39:06 PM.300 [NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
1/23/2018 22:39:06 PM.300 [NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
1/23/2018 22:39:06 PM.300 [NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
1/23/2018 22:39:06 PM.300 [NOTICE] Opening Socks listener on 127.0.0.1:9150<br />
1/23/2018 22:39:06 PM.300 [NOTICE] Renaming old configuration file to "C:\Users\XONE\Desktop\Tor Browser\Browser\TorBrowser\Data\Tor\torrc.orig.3"<br />
1/23/2018 22:39:07 PM.300 [NOTICE] Bootstrapped 5%: Connecting to directory server<br />
1/23/2018 22:39:07 PM.500 [NOTICE] Bootstrapped 10%: Finishing handshake with directory server </p>
<p>1/23/2018 22:39:15 PM.400 [WARN] 10 connections have failed:<br />
1/23/2018 22:39:15 PM.400 [WARN]  10 connections died in state handshaking (TLS) with SSL state SSLv2/v3 read server hello A in HANDSHAKE<br />
1/23/2018 22:39:15 PM.400 [NOTICE] Closing no-longer-configured Socks listener on 127.0.0.1:9150<br />
1/23/2018 22:39:15 PM.400 [NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
1/23/2018 22:39:15 PM.400 [NOTICE] Closing old Socks listener on 127.0.0.1:9150<br />
1/23/2018 22:39:16 PM.400 [NOTICE] Delaying directory fetches: DisableNetwork is set.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273545"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273545" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">January 24, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273524" class="permalink" rel="bookmark">1/23/2018 22:39:06 PM.300 …</a> by <span>jose (not verified)</span></p>
    <a href="#comment-273545">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273545" class="permalink" rel="bookmark">FWIW, I edited your post a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>FWIW, I edited your post a bit to make it more readable. It seems someone is trying to censor your usage of Tor Browser. You could try to configure a bridge during start-up to bypass that. See: <a href="https://tb-manual.torproject.org/en-US/circumvention.html" rel="nofollow">https://tb-manual.torproject.org/en-US/circumvention.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>

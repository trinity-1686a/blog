title: Arti 0.6.0 is released: bugfixes, error improvements, and preparation.
---
author: nickm
---
pub_date: 2022-08-03
---
categories: announcements
---
summary:
Arti 0.6.0 is released, and available for download.
---
body:


Arti is our ongoing project to create a working embeddable Tor client
in Rust. It’s not ready to replace the main Tor
implementation in C, but we believe that it’s the future.

Right now, our focus is on making Arti production-quality, by
stress-testing the code, hunting for likely bugs, and adding missing
features that we know from experience that users will need.  We're
going to try not to break backward compatibility too much, but we'll
do so when we think it's a good idea.

# What's new in 0.6.0?

**For a complete list of changes, have a look at the
[CHANGELOG].**

In this release, we reworked a large number of internal APIs to [simplify] and correct our code, to provide [better error messages], and provide more of Tor's [cryptographic handshakes].

We also fixed major compatibility issues on [Windows] and [iOS].

There are also a bunch of smaller features, bugfixes, and
infrastructure improvements; again, see the [CHANGELOG] for a more
complete list.

[simplify]: https://gitlab.torproject.org/tpo/core/arti/-/issues/483
[better error messages]: https://gitlab.torproject.org/tpo/core/arti/-/issues/323
[cryptographic handshakes]: https://gitlab.torproject.org/tpo/core/arti/-/merge_requests/618
[Windows]: https://gitlab.torproject.org/tpo/core/arti/-/merge_requests/627
[iOS]: https://gitlab.torproject.org/tpo/core/arti/-/merge_requests/652
[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-060-1-august-2022


# And what's next?

In the short term, we're working for feature-parity with Tor in
[netflow resistance] and [congestion control].  We already have some
backend code, and hope to land the remainder soon.

Beyond that, between now and our 1.0.0 milestone in September, we're
aiming to make Arti a production-quality Tor client for direct
internet access.  (Onion services aren't funded yet, but we hope to
change that soon.)

To do so, we need to bring Arti up to par with the C tor
implementation in terms of its [CPU usage], and [security features].
You can follow our progress on our [1.0.0 milestone].

We still plan to continue regular releases between now and then.

[netflow resistance]: https://gitlab.torproject.org/tpo/core/arti/-/issues/62
[congestion control]: https://gitlab.torproject.org/tpo/core/arti/-/issues/88
[CPU usage]: https://gitlab.torproject.org/tpo/core/arti/-/issues/87
[resiliency]: https://gitlab.torproject.org/tpo/core/arti/-/issues/329
[security features]: https://gitlab.torproject.org/tpo/core/arti/-/issues/331
[1.0.0 milestone]: https://gitlab.torproject.org/tpo/core/arti/-/issues?scope=all&state=opened&milestone_title=Arti%201.0.0%3A%20Ready%20for%20production%20use


# Here's how to try it out

We rely on users and volunteers to find problems in our software and
suggest directions for its improvement.  Although Arti isn't yet ready
for production use, you can test it as a SOCKS proxy (if you're willing
to compile from source) and as an embeddable library (if you don't mind
a little API instability).

Assuming you've installed Arti (with `cargo install arti`, or directly
from a cloned repository), you can use it to start a simple SOCKS proxy
for making connections via Tor with:

```
$ arti proxy -p 9150
```

and use it more or less as you would use the C Tor implementation!

(It doesn't support onion services yet.  If compilation doesn't work, make sure you have development files for libsqlite installed on your platform.)

If you want to build a program with Arti, you probably want to start with the [`arti-client`](https://docs.rs/arti-client/latest/arti_client/index.html) crate.  Be sure to check out the [examples](https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti-client/examples) too.

For more information, check out the [README](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md) file. (For now, it assumes that you're comfortable building Rust programs from the command line).  Our [CONTRIBUTING](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CONTRIBUTING.md) file has more information on installing development tools, and on using Arti inside of Tor Browser. (If you want to try that, please be aware that Arti doesn't support onion services yet.)

When you find bugs, please report them [on our bugtracker](https://gitlab.torproject.org/tpo/core/arti/).  You can [request an account](https://gitlab.onionize.space/) or [report a bug anonymously](https://anonticket.onionize.space/).

And if this documentation doesn't make sense, please ask questions! The questions you ask today might help improve the documentation tomorrow.

# Call for feedback

Our priority for the coming months is to make Arti a production-quality Tor client, for the purposes of direct connections to the internet.  (Onion services will come later.)  We know some of the steps we'll need to take to get there, but not all of them: we need to know what's missing _for your use-cases_.

Whether you're a user or a developer, please give Arti a try, and let us know what you think.  The sooner we learn what you need, the better our chances of getting it into an early milestone.

# Acknowledgments

Thanks to everyone who has contributed to this release, including Arturo
Marquez, Dimitris Apostolou, `feelingnothing`, Jim Newsome, Richard
Pospesel, `spongechameleon`, Trinity Pointard, and Yuan Lyu.

And thanks, of course, to [Zcash Community Grants](https://grants.zfnd.org/proposals/215972995-arti-a-pure-rust-tor-implementation-for-zcash-and-beyond) (formerly Zcash Open Major Grants (ZOMG)) for funding this project!

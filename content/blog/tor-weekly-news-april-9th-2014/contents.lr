title: Tor Weekly News — April 9th, 2014
---
pub_date: 2014-04-09
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the fourteenth issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>The Heartbleed Bug and Tor</h1>

<p>OpenSSL bug <a href="https://www.openssl.org/news/vulnerabilities.html#2014-0160" rel="nofollow">CVE-2014-0160</a>, also known as the <a href="http://heartbleed.com/" rel="nofollow">Heartbleed bug</a>, “allows anyone on the Internet to read the memory of systems protected by the vulnerable versions of the OpenSSL software”, potentially enabling the compromise of information including “user names and passwords, instant messages, emails, and business critical documents and communication”. Tor is one of the very many networking programs that use OpenSSL to communicate over the Internet, so within a few hours of the bug’s disclosure Roger Dingledine posted a <a href="https://blog.torproject.org/blog/openssl-bug-cve-2014-0160" rel="nofollow"> security advisory</a> describing how it affects different areas of the Tor ecosystem.</p>

<p><a href="https://lists.torproject.org/pipermail/tor-talk/2014-April/032602.html" rel="nofollow">“The short version is: upgrade your openssl”</a>. Tor Browser users should upgrade as soon as possible to the new <a href="https://blog.torproject.org/blog/tor-browser-354-released" rel="nofollow">3.5.4 release</a>, which includes OpenSSL 1.0.1g, fixing the vulnerability. “The browser itself does not use OpenSSL…however, this release is still considered an important security update, because it is theoretically possible to extract sensitive information from the Tor client sub-process”, wrote Mike Perry.</p>

<p>Those using a system Tor should upgrade their OpenSSL version and manually restart their Tor process. For relay operators, “best practice would be to update your OpenSSL package, discard all the files in keys/ in your DataDirectory, and restart your Tor to generate new keys”, and for hidden service administrators, “to move to a new hidden-service address at your convenience”. Clients, relays, and services using an older version of OpenSSL, including Tails, are not affected by this bug.</p>

<p>For mobile devices, Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2014-April/003383.html" rel="nofollow">called for immediate testing</a> of Orbot 13.0.6-beta-3, which not only upgrades OpenSSL but also contains a fix for the transproxy leak <a href="https://lists.torproject.org/pipermail/tor-talk/2014-March/032503.html" rel="nofollow">described by Mike Perry</a> two weeks ago, in addition to smaller fixes and improvements from <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2014-April/003375.html" rel="nofollow">13.0.6-beta-1</a> and subsequently. You can obtain a copy of the .apk file directly from the Guardian Project’s <a href="https://guardianproject.info/releases/" rel="nofollow">distribution page</a>.</p>

<p>Ultimately, “if you need strong anonymity or privacy on the Internet, you might want to stay away from the Internet entirely for the next few days while things settle.” Be sure to read Roger’s post in full for a more detailed explanation if you are unsure what this bug might mean for you.</p>

<h1>A hall of Tor mirrors</h1>

<p>Users the world over are increasingly aware of Tor’s leading reputation as a well-researched and -developed censorship circumvention tool — and, regrettably, so are censorship authorities. Events such as last month’s (short-lived) <a href="https://www.eff.org/deeplinks/2014/03/when-tor-block-not-tor-block" rel="nofollow">disruption of access to the main Tor Project website</a> from some Turkish internet connections have reaffirmed the need for multiple distribution channels that users can turn to during a censorship event in order to acquire a copy of the Tor Browser, secure their browsing, and beat the censors. One of the simplest ways of ensuring this is to make a copy of the entire website and put it somewhere else.</p>

<p>Recent days have seen the establishment of a large number of new Tor website mirrors, for which thanks must go to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000497.html" rel="nofollow">Max Jakob Maass</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000499.html" rel="nofollow">Ahmad Zoughbi</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000500.html" rel="nofollow">Darren Meyer</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000501.html" rel="nofollow">Piratenpartei Bayern</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000505.html" rel="nofollow">Bernd Fix</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000506.html" rel="nofollow">Florian Walther</a>, the <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000507.html" rel="nofollow">Electronic Frontier Foundation</a> (on a subdomain formerly housing the Tor Project’s official site), the <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000508.html" rel="nofollow">Freedom of the Press Foundation</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000509.html" rel="nofollow">Caleb Xu</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-March/000510.html" rel="nofollow">George Kargiotakis</a>, and <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-April/000512.html" rel="nofollow">Tobias Markus</a>, as well as to all the mirror operators of <a href="https://www.torproject.org/getinvolved/mirrors" rel="nofollow">longer standing</a>.</p>

<p>If you’d like to participate in the effort to render blocking of the Tor website even more futile, please see the <a href="https://www.torproject.org/docs/running-a-mirror" rel="nofollow">instructions for running a mirror</a>, and then come to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-mirrors" rel="nofollow">tor-mirrors mailing list</a> to notify the community!</p>

<h1>Mission Impossible: Hardening Android for Security and Privacy</h1>

<p>On the Tor Blog, Mike Perry posted <a href="https://blog.torproject.org/blog/mission-impossible-hardening-android-security-and-privacy" rel="nofollow">another large and comprehensive hacking guide</a>, this time describing “the installation and configuration of a prototype of a secure, full-featured, Android telecommunications device with full Tor support, individual application firewalling, true cell network baseband isolation, and optional ZRTP encrypted voice and video support.” The walkthrough covers hardware selection and setup, recommended software, Google-free backups, and disabling the built-in microphone of a Nexus 7 tablet (with a screwdriver).</p>

<p>As it stands, following this guide may require a certain level of patience, but as Mike wrote, “it is our hope that this work can be replicated and eventually fully automated, given a good UI, and rolled into a single ROM or ROM addon package for ease of use. Ultimately, there is no reason why this system could not become a full fledged off the shelf product, given proper hardware support and good UI for the more technical bits.”</p>

<p>Mike has already added to and improved parts of the guide following contributions from users in the comments beneath the post. If you would like to work (or already are working) at the cutting-edge of research into mobile device security and usability, take a look at Mike’s suggestions for future work at the bottom of the guide, and please share your ideas with the community.</p>

<h1>More monthly status reports for March 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of March continued, with submissions from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-April/000497.html" rel="nofollow">Arlo Breault</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-April/000499.html" rel="nofollow">Colin Childs</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-April/000500.html" rel="nofollow">George Kadianakis</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-April/000501.html" rel="nofollow">Michael Schloh von Bennewitz</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-April/000502.html" rel="nofollow">Philipp Winter</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-April/000503.html" rel="nofollow">Kevin Dyer</a>.</p>

<p>Arturo Filastò reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-April/000496.html" rel="nofollow">OONI team</a>, while Mike Perry did likewise for the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-April/000498.html" rel="nofollow">Tor Browser team</a>.</p>

<h1>Miscellaneous news</h1>

<p>David Goulet <a href="https://lists.torproject.org/pipermail/tor-dev/2014-April/006649.html" rel="nofollow">announced</a> the seventh release candidate for <a href="https://gitweb.torproject.org/torsocks.git" rel="nofollow">Torsocks 2.0.0</a>, the updated version of the wrapper for safely using network applications with Tor. “Nothing major, fixes and some code refactoring went in”, said David. Please review, test, and report any issues you find.</p>

<p>Nathan Freitas <a href="https://lists.torproject.org/pipermail/tor-talk/2014-April/032574.html" rel="nofollow">posted</a> a brief analysis of the role played by Orbot in the recent Turkish internet service disruption: “it might be good to think about Turkey’s Twitter block as a “censorship-lite” event, not unlike the UK or Indonesia, and then figure out how we can encourage more adoption.”</p>

<p>Jann Horn <a href="https://lists.torproject.org/pipermail/tor-relays/2014-March/004199.html" rel="nofollow">drew attention</a> to a potential issue caused by some Tor relays sending out globally-sequential IP IDs. Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-relays/2014-April/004206.html" rel="nofollow">linked</a> to an academic paper connected with the same question, while Daniel Bilik <a href="https://lists.torproject.org/pipermail/tor-relays/2014-April/004207.html" rel="nofollow">suggested</a> one method of preventing this from happening on FreeBSD. Exactly how significant this issue is (or is not) for the Tor network is very much an open question; further research into which operating systems it affects, and how it might be related to known attacks against anonymity, would be very welcome.</p>

<p>As part of their current <a href="https://pressfreedomfoundation.org/bundle/encryption-tools-journalists#donate" rel="nofollow">campaign to fund usable encryption tools</a> (including Tor) for journalists, the Freedom of the Press Foundation <a href="https://pressfreedomfoundation.org/blog/2014/04/help-support-little-known-privacy-tool-has-been-critical-journalists-reporting-nsa" rel="nofollow">published</a> a blog post on the “little-known” Tails operating system, featuring quotes from three of the journalists most prominently associated with the recent Snowden disclosures (Laura Poitras, Glenn Greenwald, and Barton Gellman) attesting to the important role Tails has played in their ability to carry out their work. If you’re impressed by what you read, please donate to the campaign — or <a href="https://tails.boum.org/contribute/index" rel="nofollow">become a Tails contributor!</a></p>

<p>Two Tor-affiliated projects — the Open Observatory of Network Interference and Tails — have each submitted a proposal to this year’s <a href="https://www.newschallenge.org" rel="nofollow">Knight News Challenge</a>. The <a href="https://www.newschallenge.org/challenge/2014/submissions/global-internet-monitoring-project" rel="nofollow">OONI proposal</a> involves further developing the ooni-probe software suite and deploying it in countries around the world, as well as working on analysis and visualization of the data gathered, in collaboration with the <a href="https://chokepointproject.net/" rel="nofollow">Chokepoint Project</a>; while <a href="https://www.newschallenge.org/challenge/2014/submissions/improve-tails-to-limit-the-impact-of-security-flaws-isolate-critical-applications-and-provide-same-day-security-updates" rel="nofollow">Tails’ submission</a> proposes to “improve Tails to limit the impact of security flaws, isolate critical applications, and provide same-day security updates”. Voting is limited to the Knight Foundation’s trustees, but feel free to read each submission and leave your comments for the developers.</p>

<p>Robert <a href="https://lists.torproject.org/pipermail/tor-dev/2014-April/006627.html" rel="nofollow">posted</a> a short proposal for “a prototype of a next-generation Tor control interface, aiming to combine the strengths of both the present control protocol and the state-of-the-art libraries”. The idea was originally destined for this year’s GSoC season, but in the end Robert opted instead to “get some feedback and let the idea evolve.”</p>

<p>After the end of the <a href="https://tails.boum.org/blueprint/logo/" rel="nofollow">Tails logo contest</a> last week, sajolida <a href="https://mailman.boum.org/pipermail/tails-dev/2014-April/005390.html" rel="nofollow">announced</a> that the winner will be declared by April 9th, after a week of voting by the most active Tails contributors.</p>

<p>Following last week’s progress on the Tor website redesign campaign, William Papper <a href="https://lists.torproject.org/pipermail/www-team/2014-April/000301.html" rel="nofollow">presented</a> a functioning <a href="http://wpapper.github.io/tor-download-web/" rel="nofollow">beta version</a> of the new download page that he and a team of contributors have been building. Have a look, and let the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/www-team" rel="nofollow">www-team list</a> know what works and what doesn’t!</p>

<p>Michael Schloh von Bennewitz began work on a <a href="https://trac.torproject.org/projects/tor/wiki/doc/TorBrowser/VMSetup" rel="nofollow">guide to configuring a virtual machine for building the Tor Browser Bundle</a>, and another to <a href="https://trac.torproject.org/projects/tor/wiki/doc/TorBrowser/BuildingWithGitian" rel="nofollow">building with Gitian</a>.</p>

<h1>Tor help desk roundup</h1>

<p>Tor Browser users often try to set a proxy when they don’t need to. Many users think they can circumvent website bans or get additional security by doing this. Discussion on clarifying the tor-launcher interface is taking place on the <a href="https://bugs.torproject.org/11405" rel="nofollow">bug tracker</a>.</p>

<h1>News from Tor StackExchange</h1>

<p>Tor’s StackExchange did its second site self-evaluation <a href="https://meta.tor.stackexchange.com/q/196/88" rel="nofollow"></a>. Users were asked to review ten questions and their respective answers. This should help to improve the site's overall quality.</p>

<p>The question <a href="https://tor.stackexchange.com/q/1573/88" rel="nofollow">“Why does GnuPG show the signature of Erinn Clark as not trusted?”</a> got the best rating. When a user verified the downloaded copy of Tor Browser Bundle, GnuPG showed Erinn’s signature as not-trusted. Jens Kubieziel explained the trust model of GnuPG in his answer, and gapz referred to the <a href="http://gnupg.org/gph/en/manual/x334.html" rel="nofollow">handbook</a>.</p>

<p>The following questions need better answers: <a href="https://tor.stackexchange.com/q/1584/88" rel="nofollow">“How to validate certificates?”</a>; <a href="https://tor.stackexchange.com/q/1439/88" rel="nofollow">“Why does Atlas sometimes show a different IP address from https://check.torproject.org?”</a>; <a href="https://tor.stackexchange.com/q/1536/88" rel="nofollow">“Site login does not persist”</a>; and <a href="https://tor.stackexchange.com/q/1587/88" rel="nofollow">“My Atlas page is blank”</a>.</p>

<p>If you know good answers to these questions, please help the users of Tor StackExchange.</p>

<p>This issue of Tor Weekly News has been assembled by harmony, Matt Pagan, qbi, Lunar, Roger Dingledine, and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>


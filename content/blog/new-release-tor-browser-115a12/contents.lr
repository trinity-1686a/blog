title: New Alpha Release: Tor Browser 11.5a12 (Android, Windows, macOS, Linux)
---
pub_date: 2022-05-31
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5a12 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a12 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a12/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-19/) to Firefox.

The full changelog since [Tor Browser 11.5a11](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Windows + OS X + Linux
  - [Bug tor-browser#40309](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40309): Avoid using regional OS locales
  - [Bug tor-browser#40912](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40912): Hide screenshots menu since we don't support it
  - [Bug tor-browser#40916](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40916): Remove the browser.download.panel.shown preference
  - [Bug tor-browser#40918](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40918): In 11.5a11 the breadcrumbs are visible on the first bootstrap
  - [Bug tor-browser#40923](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40923): Consume country code to improve error report
- All Platforms
  - [Bug tor-browser-build#40474](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40474): Bump version of pion/webrtc to v3.1.41
  - [Bug tor-browser#40967](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40967): Integrate Mozilla fix for Bug 1770137
  - [Bug tor-browser#40968](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40968): Integrate Mozilla fix for Bug 1770048
- Build System
  - All Platforms
    - [Bug tor-browser-build#40476](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40476): Add tools/signing/do-all-signing script, and other signing scripts improvements
  - Android
    - Update Go to 1.18.2
  - Windows + OS X + Linux
    - Update Go to 1.17.10

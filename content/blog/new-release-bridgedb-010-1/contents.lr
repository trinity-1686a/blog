title: New Release: BridgeDB 0.10.1
---
pub_date: 2020-05-27
---
author: phw
---
tags:

bridges
censorship circumvention
bridgedb
bridge distribution
---
categories: circumvention
---
_html_body:

<p>When ISPs or governments block access to the Tor network, our users rely on <a href="https://community.torproject.org/relay/types-of-relays/">bridges</a> to connect. With <a href="https://bridges.torproject.org">BridgeDB</a>, we tackle the problem of how to get bridges to censored users while making it difficult for censors to get all bridges.</p>
<p>We just released and deployed BridgeDB version 0.10.1. Here's what's new:</p>
<ul>
<li>Ticket <a href="https://bugs.torproject.org/30941">#30941</a>: Our email autoresponder was notoriously difficult to use and would only respond with bridges if you got the email commands right. Many people didn't, so we tried to ease the pain by making the autoresponder reply with obfs4 bridges even if the user sent an invalid request. We also simplified the language in the autoresponder's reply. Keep in mind that you need to send requests from either a Gmail or a Riseup address.</li>
<li>Ticket <a href="https://bugs.torproject.org/33631">#33631</a>: So far, BridgeDB remembered only the first distribution mechanism it ever learned for a given bridge. This means that if a bridge would change its mind and re-configure its distribution mechanism using the BridgeDistribution config option, BridgeDB would ignore it. This patch changes this behavior, so bridges can actually change their distribution mechanism.</li>
<li>Ticket <a href="https://bugs.torproject.org/31528">#31528</a>: BridgeDB used a bunch of silly phrases across its UI that made it unnecessarily difficult to translate and interact with. This patch replaced these phrases with simpler language. Thanks to Armin Huremagic (agix) for fixing this issue!</li>
<li>Ticket <a href="https://bugs.torproject.org/30946">#30946</a>: Python 2 is no longer supported, so we ported BridgeDB's code base to Python 3. This was no easy feat but thanks to Damian Johnson (atagar), who did most of the heavy lifting, we are now done!</li>
<li>Ticket <a href="https://bugs.torproject.org/33008">#33008</a>: We added an <a href="https://bridges.torproject.org/info">info page</a> to BridgeDB which explains its distribution mechanisms. <a href="https://metrics.torproject.org/rs.html">Relay Search</a> now shows a bridge's distribution mechanism and links to this info page, so bridge operators can learn more.</li>
<li>Ticket <a href="https://bugs.torproject.org/30317">#30317</a>: This patch updates our "howto" box, which explains how one adds bridges to Tor Browser. In addition to updating the instructions, this patch also links to instructions for Android.</li>
<li>Ticket <a href="https://bugs.torproject.org/33945">#33945</a>: This patch fixed a bug that broke BridgeDB's email autoresponder after a few hours or days. We believe that this was a regression that was introduced as part of our port to Python 3.</li>
<li>Ticket <a href="https://bugs.torproject.org/12802">#12802</a>: We built a small script that can test BridgeDB's email autoresponder. Our monitoring tool <a href="https://mmonit.com">monit</a> periodically runs this script and sends us an email alert if the autoresponder stopped working.</li>
<li>Ticket <a href="https://bugs.torproject.org/17548">#17548</a>: BridgeDB's email autoresponder used to be able to send PGP-signed messages. This feature broke a long time ago and the cost of maintaining it outweighs its usefulness, so we removed it.</li>
<li>Ticket <a href="https://bugs.torproject.org/31967">#31967</a>: BridgeDB now uses a CSPRNG to select a cached CAPTCHA. Thanks to Armin Huremagic (agix) for fixing this issue!</li>
<li>Ticket <a href="https://bugs.torproject.org/29686">#29686</a>: We renamed source code files to prevent file name conflicts on file systems that are not case sensitive.</li>
<li>Ticket <a href="https://bugs.torproject.org/34154">#34154</a>: We added several new fields to our SQLite database that we will use to keep track of where bridges are blocked.</li>
</ul>
<p>Hopefully, these changes make the lives of our users easier. Please let us know what you think and share your experience with BridgeDB!</p>

---
_comments:

<a id="comment-287992"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287992" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>my name (not verified)</span> said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
    <a href="#comment-287992">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287992" class="permalink" rel="bookmark">Hi,
why is DARP involved in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>why is DARP involved in the project when the NSA is trying to take it down?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287999"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287999" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  agix
  </article>
    <div class="comment-header">
      <p class="comment__submitted">agix said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287992" class="permalink" rel="bookmark">Hi,
why is DARP involved in…</a> by <span>my name (not verified)</span></p>
    <a href="#comment-287999">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287999" class="permalink" rel="bookmark">Hi, 
DARPA was and still is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, </p>
<p>DARPA was and still is one of the main funders for the Tor project (see the full list here: <a href="https://www.torproject.org/about/sponsors/" rel="nofollow">https://www.torproject.org/about/sponsors/</a>) since they continued the development of onion routing back in the 90's.<br />
I believe one reason why DARPA is still sponsoring the Tor project is that they support efforts to circumvent censorship.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288017"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288017" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287999" class="permalink" rel="bookmark">Hi, 
DARPA was and still is…</a> by agix</p>
    <a href="#comment-288017">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288017" class="permalink" rel="bookmark">Well at least DARPA supports…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well at least DARPA supports efforts to circumvent censorship. Would be nice if the Tor Blog behaved the same.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-287994"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287994" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Omom Hoh (not verified)</span> said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
    <a href="#comment-287994">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287994" class="permalink" rel="bookmark">How to join</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How to join</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288000"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288000" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  agix
  </article>
    <div class="comment-header">
      <p class="comment__submitted">agix said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287994" class="permalink" rel="bookmark">How to join</a> by <span>Omom Hoh (not verified)</span></p>
    <a href="#comment-288000">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288000" class="permalink" rel="bookmark">The best way to join the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The best way to join the community is by chatting in the OFTC IRC network.<br />
<a href="https://support.torproject.org/get-in-touch/#irc-help/" rel="nofollow">Here</a> is a short summary on how to join.<br />
<a href="https://www.torproject.org/contact/" rel="nofollow">Here</a> is a list of all the interesting channels.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287995"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287995" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Naël (not verified)</span> said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
    <a href="#comment-287995">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287995" class="permalink" rel="bookmark">Hello, I&#039;m French and I&#039;m…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello, I'm French and I'm just starting out but I can't connect.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288002"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288002" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  agix
  </article>
    <div class="comment-header">
      <p class="comment__submitted">agix said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287995" class="permalink" rel="bookmark">Hello, I&#039;m French and I&#039;m…</a> by <span>Naël (not verified)</span></p>
    <a href="#comment-288002">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288002" class="permalink" rel="bookmark">Hey,
maybe you will find…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey,<br />
maybe you will find your answer <a href="https://support.torproject.org/connecting/" rel="nofollow">here</a>.<br />
If you are still facing issues, reach out to the community by joining them in the OFTC IRC network (<a href="https://support.torproject.org/get-in-touch/#irc-help" rel="nofollow">Here</a> is a short guide) and they will be happy to help you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287997"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287997" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jonny (not verified)</span> said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
    <a href="#comment-287997">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287997" class="permalink" rel="bookmark">Its my First time here. I am…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Its my First time here. I am so exited</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288003"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288003" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jajajano (not verified)</span> said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
    <a href="#comment-288003">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288003" class="permalink" rel="bookmark">mi pais bloquea el acceso de…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>mi pais bloquea el acceso de tor y no me permite instalarlo como navegador</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288007"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288007" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  agix
  </article>
    <div class="comment-header">
      <p class="comment__submitted">agix said:</p>
      <p class="date-time">June 01, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288003" class="permalink" rel="bookmark">mi pais bloquea el acceso de…</a> by <span>jajajano (not verified)</span></p>
    <a href="#comment-288007">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288007" class="permalink" rel="bookmark">Puedes recibir una copia del…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Puedes recibir una copia del navegador Tor mediante Gettor (<a href="https://gettor.torproject.org" rel="nofollow">en el link</a> hay más informaciones sobre Gettor) Tienes que enviar una petición a <a href="mailto:gettor@torproject.org" rel="nofollow">gettor@torproject.org</a> en el que especificas tu sistema operativo y tu lugar.<br />
Por ejemplo: „windows es“ y Gettor te responderá con links para descargarte el navegador Tor.<br />
Puedes también intentar descargarte el navegador Tor de este link <a href="https://tor.eff.org " rel="nofollow">tor.eff.org</a> o de <a href="https://tor.ccc.de" rel="nofollow">tor.ccc.de</a> (para ver toda la lista usa este link <a href="https://2019.www.torproject.org/getinvolved/mirrors.html.en" rel="nofollow">mirros</a>).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-288198"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288198" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 07, 2020</p>
    </div>
    <a href="#comment-288198">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288198" class="permalink" rel="bookmark">It wouldn&#039;t hurt to have a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It wouldn't hurt to have a basic intro of what BridgeDB is and what it's for. The <a href="https://bridges.torproject.org" rel="nofollow">https://bridges.torproject.org</a> homepage just explains how to use it, and the /info page just covers the distribution mechanisms. I couldn't find an overview or FAQ about BridgeDB anywhere.</p>
<p>e.g., To give you some ideas:<br />
- What is BridgeDB? Who uses it? Should I care?<br />
- Is there only one central BridgeDB instance (bridges.torproject.org) or are there third-party instances in existence? Where can I find them?<br />
- If I wanted to operate a bridge relay, how would I use BridgeDB to advertise my bridge? Does it happen automatically, and can I opt-out? How do I advertise my bridge on a third-party BridgeDB instance?<br />
- I don't want it to be publicly known that I'm running a bridge relay from my IP address. (e.g., Use of Tor is illegal or suspicious in my country.) Is my identity protected from scrapers and dataminers?<br />
- I don't trust Google (via Gmail, reCaptcha). Can I limit which distribution mechanisms can be used to obtain information about my bridge?<br />
- What prevents an evil government from flooding the BridgeDB with fake addresses, or poisoning it with honeypot bridges?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288227"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288227" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  agix
  </article>
    <div class="comment-header">
      <p class="comment__submitted">agix said:</p>
      <p class="date-time">June 09, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288198" class="permalink" rel="bookmark">It wouldn&#039;t hurt to have a…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288227">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288227" class="permalink" rel="bookmark">Thanks for the helpful…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the helpful feedback!<br />
We will work on answering your questions and provide additional information about BridgeDB at <a href="support.torproject.org" rel="nofollow">support.torproject.org</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-288409"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288409" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>nonickname (not verified)</span> said:</p>
      <p class="date-time">June 29, 2020</p>
    </div>
    <a href="#comment-288409">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288409" class="permalink" rel="bookmark">hello，although I have used…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello，although I have used the bridge which provideed by Bridge DB in China， I still can't connect to tor.However，sometimes it's strange that I do can connect to tor ，is it normal？Or is the GFW or the government has blocked some of the tor relay？</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288410"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288410" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">June 30, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288409" class="permalink" rel="bookmark">hello，although I have used…</a> by <span>nonickname (not verified)</span></p>
    <a href="#comment-288410">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288410" class="permalink" rel="bookmark">I suggest joining the #tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I suggest joining the #tor IRC channel at irc.oftc.net. People will be able to help you out with bridges.</p>
<p>Past research has shown that the GFW occasionally has brief outages during which users may be able to use the Tor network but in general, the GFW is blocking all relays.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>

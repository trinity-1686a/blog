title: EFF and Tor in Google Summer of Code 2009
---
pub_date: 2009-03-21
---
author: karsten
---
tags:

gsoc
Google Summer of Code
gsoc 2009
---
categories: internships
---
_html_body:

<p>Great news! We have been accepted as a mentoring organization for <a href="http://socghop.appspot.com/program/home/google/gsoc2009" rel="nofollow">Google Summer of Code 2009</a> together with <a href="https://www.eff.org/" rel="nofollow">The Electronic Frontier Foundation</a>. Yay!</p>

<p>This will be our third Google Summer of Code after 2007 and <a href="https://blog.torproject.org/blog/tor-project-google-summer-code-2008%21" rel="nofollow">2008</a>. In our <a href="http://code.google.com/soc/2007/eff/about.html" rel="nofollow">first year</a> we had four students working on making relays work better (and not crash) on Windows, on a library and tool to try alternative path selection algorithms, on a fuzzing library to look for parsing problems, and on scalability and privacy for hidden services. In our <a href="http://code.google.com/soc/2008/eff/about.html" rel="nofollow">second year</a> we had seven students. One of our successful students of the 2008 program wrote a nice <a href="https://blog.torproject.org/blog/google-summer-code-2008-review" rel="nofollow">blog post</a> reviewing how GSoC went for him, for the other students, and for the project in general.</p>

<p>We have made resolutions for our third GSoC participation to make it even more successful than in the past years. We have set ourselves the limit of accepting no more than 4 students (plus up to 2 students mentored by EFF). Rather than being persuaded by all those great applications, we want to focus on the most promising projects and students. We plan to assign up to 3 mentors to each student to provide optimal support. We will try harder to encourage students to interact with the community and become a part of it. It may be challenging to discuss project ideas on a mailing list or in a chat room with dozens or hundreds of unknown people listening. But communication is an important part of the GSoC experience (if not the most important).</p>

<p>It might seem that we have high expectations about our students (actually we do!), but we also have a lot to offer. Our students will work on one of the largest deployed anonymity systems with a few hundred thousand users. We encourage GSoC students to stick to The Tor Project after summer and become part of the team; did you know that two of our <a href="https://www.torproject.org/people#Core" rel="nofollow">Core people</a> are former GSoC students? Our mentors are cryptographers, researchers, and/or software developers who have published numerous papers on distinguished security and privacy conferences all over the world. We have an awesome community with a lot of people who will be interested in the students' projects and their progress. Ah, and finally Google gratefully pays $4.5K to each successful GSoC student. Isn't that a fine alternative to $your_last_summer_job?</p>

<p>Did we get you interested? These are your next steps: Look at Google's FAQ whether you are <a href="http://socghop.appspot.com/document/show/program/google/gsoc2009/faqs#eligibility" rel="nofollow">eligible</a> to participate. Have a look at our <a href="https://www.torproject.org/gsoc#Ideas" rel="nofollow">ideas lists</a> (one for <a href="https://www.torproject.org/volunteer.html.en#Projects" rel="nofollow">The Tor Project</a>, one for the <a href="http://switzerland.wiki.sourceforge.net/Projects" rel="nofollow">EFF's Switzerland tool</a>). Put some thoughts on the listed ideas, or make up your own. Talk to us. No, really, this step is important: Talk to us! Come to <a href="irc://irc.oftc.net/tor/" rel="nofollow">#tor on irc.oftc.net</a> or send us an <a href="mailto:tor-assistants@freehaven.net" rel="nofollow">email</a> and let us know about your project idea. Write down your application using our <a href="https://www.torproject.org/gsoc#Template" rel="nofollow">template</a> and send it in until <a href="http://socghop.appspot.com/document/show/program/google/gsoc2009/faqs#timeline" rel="nofollow">April 3, 2009, 19:00 UTC</a>. Stick around for any questions we might have to you. We are looking forward to reading your application, erm, to talking to you and discussing your project idea!</p>

---
_comments:

<a id="comment-856"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-856" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous Coward ;) (not verified)</span> said:</p>
      <p class="date-time">March 21, 2009</p>
    </div>
    <a href="#comment-856">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-856" class="permalink" rel="bookmark">Sorry for posting something</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry for posting something OT.<br />
But I found this (half year old) article:<br />
<a href="http://news.cnet.com/8301-13578_3-10040152-38.html" rel="nofollow">http://news.cnet.com/8301-13578_3-10040152-38.html</a><br />
Which states that China and the US are trying to develop some kind of ip traceback system. How seriously should one take this article? I mean if such a system ever came into existence wouldn't it make almost anonymity impossible? I thought you at tor project could answer these questions. I don't want to try spread this information if it isn't a serious threat because there are so many other threats to anonymity online which people need to know about.</p>
</div>
  </div>
</article>
<!-- Comment END -->

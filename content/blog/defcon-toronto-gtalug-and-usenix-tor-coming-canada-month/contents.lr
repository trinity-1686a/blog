title: DEFCON Toronto, GTALUG, and USENIX: Tor is Coming to Canada This Month
---
pub_date: 2017-08-04
---
author: steph
---
tags: conference
---
categories: community
---
summary:

Sukhbir Singh is a software developer in the applications and community team of the Tor project since 2012, and at two talks this month in Toronto, Sukhbir will introduce Tor and then walk through the Tor ecosystem: all of the applications build around it. 
---
_html_body:

<p dir="ltr">When people talk about Tor, they almost always think of the Tor daemon or Tor Browser. While this was true earlier, the Tor ecosystem has now expanded and includes a diverse set of applications helping you protect your privacy online or to circumvent Internet censorship.</p>
<h3 dir="ltr">Learn About the Tor Ecosystem in Toronto</h3>
<p dir="ltr">Sukhbir Singh is a software developer in the applications and community team and has been with us since 2012. At two talks this month in Toronto, Sukhbir will introduce Tor and then walk through the Tor ecosystem: all of the applications build around it. These talks are appropriate for Tor beginners and advanced users.</p>
<p dir="ltr">Learn about the Tor ecosystem from Sukhbir Singh at:</p>
<ul>
<li dir="ltr">
<p dir="ltr"><a href="https://gtalug.org/meeting/2017-08/">GTALUG</a>, Tuesday, August 8, 7:30 PM</p>
</li>
<li dir="ltr">
<p dir="ltr"><a href="https://www.meetup.com/DEFCON416/events/240968477/">DEFCON Toronto</a>, Wednesday, August 9, 2017, 6:00 PM</p>
</li>
</ul>
<h3 dir="ltr">Tor in Vancouver</h3>
<p dir="ltr">For the academics, there will be two research projects presented at the <a href="https://www.usenix.org/conference/usenixsecurity17/technical-sessions">26th USENIX Security Symposium </a>Wednesday, August 16th. The USENIX Security Symposium brings together researchers, practitioners, system administrators, system programmers, and others interested in the latest advances in the security and privacy of computer systems and networks.</p>
<ul>
<li dir="ltr">
<p dir="ltr">Characterizing the Nature and Dynamics of Tor Exit Blocking<br />
    <em>Rachee Singh, University of Massachusetts – Amherst; Rishab Nithyanand, Stony Brook University; Sadia Afroz, University of California, Berkeley and International Computer Science Institute; Paul Pearce, UC Berkeley; Michael Carl Tschantz, International Computer Science Institute; Phillipa Gill, University of Massachusetts – Amherst; Vern Paxson, University of California, Berkeley and International Computer Science Institute</em></p>
</li>
<li dir="ltr">
<p dir="ltr">DeTor: Provably Avoiding Geographic Regions in Tor<br />
    <em>Zhihao Li, Stephen Herwig, and Dave Levin, University of Maryland</em></p>
</li>
</ul>
<h3 dir="ltr">Find a Tor Talk Near You</h3>
<p dir="ltr">Tor folks appear at talks around the world. Check our <a href="https://blog.torproject.org/events/month">event page</a> to find an event near you.</p>
<p>
 </p>

---
_comments:

<a id="comment-270319"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270319" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>hulksmash (not verified)</span> said:</p>
      <p class="date-time">August 08, 2017</p>
    </div>
    <a href="#comment-270319">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270319" class="permalink" rel="bookmark">Where is the fun</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where is the fun<strong></strong></p>
</div>
  </div>
</article>
<!-- Comment END -->

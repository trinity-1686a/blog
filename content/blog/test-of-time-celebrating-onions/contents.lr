title: Test of Time: Celebrating Onions
---
pub_date: 2020-05-18
---
author: alsmith
---
summary:

Today, the pre-Tor onion routing paper, "Anonymous Connections and Onion Routing" by Paul Syverson, David Goldschlag, and Michael Reed from IEEE S&P 1997, is receiving the Test of Time Award by the IEEE Symposium on Security and Privacy in Oakland. Congratulations! 
---
_html_body:

<p>Today, the pre-Tor onion routing paper, "Anonymous Connections and Onion Routing" by Paul Syverson, David Goldschlag, and Michael Reed from IEEE S&amp;P 1997, is receiving the Test of Time Award by the IEEE Symposium on Security and Privacy in Oakland.</p>
<p>This award recognizes papers published at IEEE’s flagship security conference that have made a lasting impact on the field, and so has "Anonymous Connections and Onion Routing." This work introduced many ideas that would later be important for Tor’s design.</p>
<p>"Anonymous Connections and Onion Routing" was the first to mention what would become the default configuration for Tor, where clients build onions locally but are not themselves on onion routers, moving away from the limitations of a peer-to-peer model.</p>
<p>This design was the first description and performance analysis of an onion routing implementation, one that was publicly available and usable starting in 1996. It introduced padding cells to counter traffic analysis and contained description and protocols for setup and maintenance of a dynamic network of onion routers.</p>
<p>We would like to take a moment to congratulate all the researchers who have been working on privacy and anonymity, in particular those who developed "Anonymous Connections and Onion Routing" for their award today. The main reason the Tor Project can provide such a high standard of privacy and anonymity to millions of people around the world is because we can count on scientific support from hundreds of researchers that have contributed to our work. Without this community, it would be much more difficult for us to 1. Be aware of problems and 2. Understand how to solve these problems.</p>
<p>Test of Time awards are a great recognition of the importance of this work. To show a small gesture in their honor, we would like to share a selection of Test of Time awards received by papers that have influenced our work at Tor:</p>
<ul>
<li>"Proxies for Anonymous Routing" by Michael G. Reed, Paul F. Syverson, and David M. Goldschlag from ACSAC '96 - received the Test of Time Award at the Annual Computer Security Applications Conference (ACSAC) in December 2019.</li>
<li>"Group Principals and the Formalization of Anonymity" published at FM 99 - received the Lucas Award (which is a Test of Time Award given every three years) at the World Congress on Formal Methods (FM) in September 2019.</li>
<li>"Tor: The Second-Generation Onion Router" by Roger Dingledine, Nick Mathewson, and Paul Syverson published in the Proceedings of the 13th USENIX Security Symposium, August 2004 <em><strong>(this paper is what became Tor’s design)</strong></em> - received the Test of Time Award at USENIX SECURITY '14.</li>
</ul>
<p> </p>


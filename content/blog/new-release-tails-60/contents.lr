title: New Release: Tails 6.0
---
pub_date: 2024-02-27
---
author: tails
---
categories:

partners
releases
---
summary:

We are very excited to present you Tails 6.0, the first version of Tails based
on Debian 12 (Bookworm) and GNOME 43. Tails 6.0 brings new versions of most of
the software included in Tails as well as several important security and
usability improvements.

---
body:

## New features

### Error detection on the Persistent Storage

Tails 6.0 warns you about [errors when reading or
writing](https://tails.net/ioerror/) from your Tails USB stick.

These alerts can help you diagnose hardware failures on your USB stick and
backup your Persistent Storage before it's too late.

### Mount external devices automatically

When you plug in an external storage device, a USB stick or an external hard
disk, Tails 6.0 mounts it automatically. If the storage device contains an
encrypted partition, Tails 6.0 offers you to unlock the encryption
automatically.

This feature also simplifies how to unlock [_VeraCrypt_ encrypted
volumes](https://tails.net/doc/encryption_and_privacy/veracrypt/).

### Protection against malicious USB devices

If an attacker manages to plug a [malicious USB
device](https://en.wikipedia.org/wiki/BadUSB) in your computer, they could run
software that breaks the security built in Tails without your knowledge.

To protect from such attacks while you are away from your computer, Tails 6.0
ignores any USB device that is plugged in while your screen is locked.

You can only use new USB devices if they are plugged in while the screen is
unlocked.

### Dark Mode and Night Light

From the system menu of Tails 6.0, you can now switch between:

  * The default _light_ mode with colder colors and more brightness
  * A _dark_ mode
  * A _night light_ mode with warmer colors and less brightness
  * A combination of both the _dark_ mode and _night light_ mode

### Easier screenshots and screencasts

GNOME 43 introduces a new shortcut in the system menu that makes it easier to
take a screenshot or record a screencast.

### Easier Gmail in Thunderbird

Thanks to changes in both Thunderbird and Gmail, it's much easier to configure
a Gmail account in Thunderbird in Tails 6.0.

  * You don't have to configure anything special in your Gmail account, other than the usual 2-Step Verification.
  * You can sign in to your Gmail account directly when configuring it in _Thunderbird_.

### Diceware passphrases in 5 more languages

When creating a Persistent Storage, suggested passphrases are now also
generated in Catalan, German, Italian, Portuguese, and Spanish.

Thanks to [jawlensky](https://gitlab.tails.boum.org/jawlensky) who created the
word lists for Catalan, Italian, and Spanish for Tails, but also made them
available to all users of `diceware`.

## Changes and updates

### Included software

Tails 6.0 updates most of the applications included in Tails, among others:

  * _Tor Browser_ to [13.0.10](https://blog.torproject.org/new-release-tor-browser-13010/).
  * _Electrum_ from 4.0.9 to 4.3.4
    - Improve support for the Lightning protocol and hardware wallets.
  * _KeePassXC_ from 2.6.2 to 2.7.4
    - Add entry tags.
    - Support dark mode.
    - Redesign history view.
  * _Metadata Cleaner_ from 1.0.2 to 2.4.0
    - Redesign the whole user interface.
    - Support dark mode.
    - Add support for AIFF and HEIC files.
  * _Text Editor_ from `gedit` to `gnome-text-editor`
    - Support dark mode.
  * _Inkscape_ from 1.0.2 to 1.2.2
  * _Audacity_ from 2.4.2 to 3.2.4
  * _Gimp_ from 2.10.22 to 2.10.34
  * _Kleopatra_ from 4:20.08 to 4:22.12

### Removed features

  * Remove the icons on the desktop.

The extension of GNOME Shell that we used to provide this feature is not well
integrated into GNOME and created other problems.
([#19920](https://gitlab.tails.boum.org/tails/tails/-/issues/19920))

  * Remove the item **Wipe** and **Wipe available disk space** from the shortcut menu of the _Files_ browser.

Secure deletion is not reliable enough on USB sticks and Solid-State Drives
(SSDs) for us to keep advertising this feature.

We updated our documentation on [secure
deletion](https://tails.net/doc/encryption_and_privacy/secure_deletion/) to
new recommendations: use encrypted volumes, overwrite the entire device, or
disintegrate it physically.

  * Remove the item **Remove metadata** from the shortcut menu of the _Files_ browser.

The developers of MAT2, the metadata removal library used by _Metadata
Cleaner_ are not providing this option anymore.

  * Remove _GtkHash_

You can still install _GtkHash_ as [Additional
Software](https://tails.net/doc/persistent_storage/additional_software/).

## Fixed problems

  * Fix several issues with special characters and non-Latin scripts in the screen keyboard. ([#18076](https://gitlab.tails.boum.org/tails/tails/-/issues/18076))

For more details, read our
[changelog](https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog).

## Known issues

  * _OnionShare_ is still included as version 2.2.

We tried to include _OnionShare_ 2.6 in Tails 6.0, but it has several issues
that had security implications.
([#20135](https://gitlab.tails.boum.org/tails/tails/-/issues/20135) and
[#20140](https://gitlab.tails.boum.org/tails/tails/-/issues/20140))

  * Mounting external devices automatically interferes with the _Back Up Persistent Storage_ utility. ([#20143](https://gitlab.tails.boum.org/tails/tails/-/issues/20143))

## Get Tails 6.0

### To upgrade your Tails USB stick and keep your Persistent Storage

  * Automatic upgrades are only available from Tails 6.0~rc1 to 6.0.

All other users have to do a [manual
upgrade](https://tails.net/doc/upgrade/#manual).

### To install Tails 6.0 on a new USB stick

Follow our installation instructions:

  * [Install from Windows](https://tails.net/install/windows/)

  * [Install from macOS](https://tails.net/install/mac/)

  * [Install from Linux](https://tails.net/install/linux/)

  * [Install from Debian or Ubuntu using the command line and GnuPG](https://tails.net/install/expert/)

The Persistent Storage on the USB stick will be lost if you install instead of
upgrading.

### To download only

If you don't need installation or upgrade instructions, you can download Tails
6.0 directly:

  * [For USB sticks (USB image)](https://tails.net/install/download/)

  * [For DVDs and virtual machines (ISO image)](https://tails.net/install/download-iso/)

## Support and feedback

For support and feedback, visit the [Support
section](https://tails.net/support/) on the Tails website.

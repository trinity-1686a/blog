title: Stem Release 1.3
---
pub_date: 2014-12-22
---
author: atagar
---
tags: stem
---
_html_body:

<p>Greetings wonderful people of the world! After months down in the engine room I'm delighted to announce the 1.3.0 release of Stem.</p>

<p>For those who aren't familiar with it, Stem is a Python library for interacting with Tor. With it you can script against your relay, descriptor data, or even write applications similar to arm and Vidalia.</p>

<p><b><a href="https://stem.torproject.org/" rel="nofollow">https://stem.torproject.org/</a></b></p>

<p>So what's new in this release?</p>

<h2>Better Hidden Service Support</h2>

<p>Now it's easier than ever to spin up hidden services!</p>

<p>Thanks to contributions from Federico Ceratto and Patrick O'Doherty we now have a set of methods specifically for working with hidden services. Check it out in our new tutorial...</p>

<p><b><a href="https://stem.torproject.org/tutorials/over_the_river.html" rel="nofollow">Over the River and Through the Wood</a></b></p>

<h2>Faster Descriptor Parsing</h2>

<p>This release dramatically improves the speed at which Stem can parse decriptors. Thanks to optimizations from Nick Mathewson and Ossi Herrala we can now read descriptors 40% faster!</p>

<p>This is just the tip of the iceberg. For a full rundown on the myriad of improvements and fixes in this release see...</p>

<p><b><a href="https://stem.torproject.org/change_log.html#version-1-3" rel="nofollow">https://stem.torproject.org/change_log.html#version-1-3</a></b></p>

<p>Cheers! -Damian</p>

---
_comments:

<a id="comment-84560"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84560" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2014</p>
    </div>
    <a href="#comment-84560">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84560" class="permalink" rel="bookmark">hello dear TOR guys
I had a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello dear TOR guys<br />
I had a question: is that safe to add this add-on on my TOR? do you guys -as web security experts- recommend that? <a href="https://services.addons.mozilla.org/en-US/firefox/discovery/addon/security-plus/" rel="nofollow">https://services.addons.mozilla.org/en-US/firefox/discovery/addon/secur…</a><br />
thanks for all your efforts to make web safer and more reachable for us.<br />
p.s. I apologize for irrelevant comment. I didn't know where the right place is to ask these kind of questions. sorry :)<br />
a big fan from Iran</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-84617"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84617" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 22, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-84560" class="permalink" rel="bookmark">hello dear TOR guys
I had a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-84617">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84617" class="permalink" rel="bookmark">I looked at it very very</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I looked at it very very briefly. My guess is that there's a good chance it bypasses Tor and uploads everything that you visit to a bunch of different antivirus companies. If that's true, it sounds like a terrible idea to use.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-84694"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84694" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-84694">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84694" class="permalink" rel="bookmark">wow! I have more than 10</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>wow! I have more than 10 add-ons on my TOR, some for comfort, some for security! I don't know how many of them -as you told now- are bypassing my TOR and reducing my security! add-ons like "down them all", "flash-got", "download manager(s3)", "aniweather", "ad block plus", "scrapbook", "screenshoter","quick translator", ... .<br />
anyway, thanks a lot again TOR guys</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-84648"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84648" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-84560" class="permalink" rel="bookmark">hello dear TOR guys
I had a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-84648">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84648" class="permalink" rel="bookmark">Rather than attaching</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Rather than attaching questions to random blog posts, it is better to ask at  <a href="https://tor.stackexchange.com/" rel="nofollow">https://tor.stackexchange.com/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-84583"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84583" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2014</p>
    </div>
    <a href="#comment-84583">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84583" class="permalink" rel="bookmark">Is this got real?
Comcast</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is this got real?</p>
<p>Comcast tells users to stop using Tor</p>
<p><a href="http://www.deepdotweb.co/2014/09/13/comcast-declares-war-tor/" rel="nofollow">http://www.deepdotweb.co/2014/09/13/comcast-declares-war-tor/</a></p>
<p><a href="http://www.inquisitr.com/1474279/comcast-tells-customers-to-stop-using-tor-browser/" rel="nofollow">http://www.inquisitr.com/1474279/comcast-tells-customers-to-stop-using-…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-84616"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84616" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 22, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-84583" class="permalink" rel="bookmark">Is this got real?
Comcast</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-84616">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84616" class="permalink" rel="bookmark">No, this was a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, this was a misunderstanding from months ago. Keep looking and you'll find some statements from Comcast saying no, they like Tor and they even use it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-84656"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84656" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2014</p>
    </div>
    <a href="#comment-84656">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84656" class="permalink" rel="bookmark">i have two question:
I-</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i have two question:</p>
<p>I- would like to know could   the cyber police track through hardware?if "YES" how ?by mac address?</p>
<p>2-It is true that every user has an algorithm to own ISP?<br />
Suppose I leave a comment on the site(using Tor ) They identify the IP is Related to Tor then They go in search of who  have used Tor in that time?</p>
<p>i really wonder how  someone is arrested while using tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-84791"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84791" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 23, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-84656" class="permalink" rel="bookmark">i have two question:
I-</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-84791">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84791" class="permalink" rel="bookmark">Most of these questions here</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Most of these questions here and below aren't Stem questions.</p>
<p>I'll echo the suggestion made by another user here: these sorts of questions are exactly what <a href="https://tor.stackexchange.com/" rel="nofollow">https://tor.stackexchange.com/</a> is designed for.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-84835"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84835" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-84835">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84835" class="permalink" rel="bookmark">thanks buddy. i didn&#039;t knew</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks buddy. i didn't knew this site. i'll ask my questions henceforth there :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-84664"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84664" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2014</p>
    </div>
    <a href="#comment-84664">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84664" class="permalink" rel="bookmark">sounds cool！ BTW what if I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>sounds cool！ BTW what if I leave a post on a website without SSL encrypted via the latest Tor browser leaving JS on.... Will those government's lapdog be able to excavate, exhume and dredge me out by my IP address?<br />
Hope can help.<br />
Thank you~</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-84691"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84691" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2014</p>
    </div>
    <a href="#comment-84691">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84691" class="permalink" rel="bookmark">Hello,
is it possible to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,</p>
<p>is it possible to implement VPN on TOR. Virtual Private Networks. thanks a lot</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-84727"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-84727" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2014</p>
    </div>
    <a href="#comment-84727">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-84727" class="permalink" rel="bookmark">This is indeed good news.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is indeed good news. Although this post won't make such an impression as the one before it, it is good to see that Tor Project is actively developing it's several applications and making the entire Tor Ecosystem better, faster, and easier to use.</p>
<p>Sad how funding can sometimes be disproportional. A lot of people want to pay for people in China to use Tor, but apparently there is not a lot of people willing to pay for people in EVERYWHERE to use instant messaging and email (considering how little funding and therefore development TIMB and Torbirdy have received).</p>
<p>Is it a stupid idea to try crowdfunding in those projects? Maybe I don't see the whole picture.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-85010"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-85010" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 26, 2014</p>
    </div>
    <a href="#comment-85010">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-85010" class="permalink" rel="bookmark">Why is there a sudden</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why is there a sudden increase in the number of relays from 6000+ to 10000+ in a matter of a day?.<br />
And while starting up for first time during the "loading information" stage I got a malware popup sighting one of the relays.<br />
There is no information about these anywhere here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-85138"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-85138" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-85010" class="permalink" rel="bookmark">Why is there a sudden</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-85138">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-85138" class="permalink" rel="bookmark">Yep. It wasn&#039;t a big deal --</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yep. It wasn't a big deal -- the relays would have been a threat if we'd let them stick around long enough, but we didn't.</p>
<p><a href="http://www.twitlonger.com/show/n_1sjg365" rel="nofollow">http://www.twitlonger.com/show/n_1sjg365</a></p>
<p>"This looks like a regular attempt at a Sybil attack: the attackers have signed up<br />
many new relays in hopes of becoming a large fraction of the network.<br />
But even though they are running thousands of new relays, their relays<br />
currently make up less than 1% of the Tor network by capacity. We are<br />
working now to remove these relays from the network before they become<br />
a threat, and we don't expect any anonymity or performance effects based<br />
on what we've seen so far."</p>
<p>There was also some discussion on the tor-talk mailing list.</p>
<p>(As for your 'malware sighting', it sounds like a problem with your antivirus system. See also <a href="https://www.torproject.org/docs/faq#VirusFalsePositives" rel="nofollow">https://www.torproject.org/docs/faq#VirusFalsePositives</a> )</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>

title: New Release: Tor 0.4.3.5
---
pub_date: 2020-05-15
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>After months of work, we have a new stable release series! If you build Tor from source, you can download the source code for 0.4.3.5 <a href="https://www.torproject.org/download/tor/">on the website</a>. Packages, including a new Tor Browser, should be available within the next several weeks.</p>
<p>Tor 0.4.3.5 is the first stable release in the 0.4.3.x series. This series adds support for building without relay code enabled, and implements functionality needed for OnionBalance with v3 onion services. It includes significant refactoring of our configuration and controller functionality, and fixes numerous smaller bugs and performance issues.</p>
<p>Per our support policy, we support each stable release series for nine months after its first stable release, or three months after the first stable release of the next series: whichever is longer. This means that 0.4.3.x will be supported until around February 2021--later, if 0.4.4.x is later than anticipated.</p>
<p>Note also that support for 0.4.1.x is about to end on May 20 of this year; 0.4.2.x will be supported until September 15. We still plan to continue supporting 0.3.5.x, our long-term stable series, until Feb 2022.</p>
<p>Below are the changes since 0.4.3.4-rc. For a complete list of changes since 0.4.2.6, see the ReleaseNotes file.</p>
<h2>Changes in version 0.4.3.5 - 2020-05-15</h2>
<ul>
<li>Minor bugfixes (compiler compatibility):
<ul>
<li>Avoid compiler warnings from Clang 10 related to the use of GCC- style "/* falls through */" comments. Both Clang and GCC allow __attribute__((fallthrough)) instead, so that's what we're using now. Fixes bug <a href="https://bugs.torproject.org/34078">34078</a>; bugfix on 0.3.1.3-alpha.</li>
<li>Fix compilation warnings with GCC 10.0.1. Fixes bug <a href="https://bugs.torproject.org/34077">34077</a>; bugfix on 0.4.0.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Stop truncating IPv6 addresses and ports in channel and connection logs. Fixes bug <a href="https://bugs.torproject.org/33918">33918</a>; bugfix on 0.2.4.4-alpha.</li>
<li>Fix a logic error in a log message about whether an address was invalid. Previously, the code would never report that onion addresses were onion addresses. Fixes bug <a href="https://bugs.torproject.org/34131">34131</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-287873"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287873" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>worker (not verified)</span> said:</p>
      <p class="date-time">May 15, 2020</p>
    </div>
    <a href="#comment-287873">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287873" class="permalink" rel="bookmark">Thanks for all your hard…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for all your hard work on Tor! Is WTF-PAD still coming soon?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287876"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287876" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 15, 2020</p>
    </div>
    <a href="#comment-287876">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287876" class="permalink" rel="bookmark">@ nickm: Very glad to know…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ nickm: Very glad to know you are still working for TP!  The recent announcement from the ED was worrying.</p>
<p>Despite the deteriorating fiscal situation, I hope TP can somehow assign someone to work with Debian Project to check on the health of the onion mirrors of the Debian software repository, which do not appear to be working.  This was a fabulous innovation several years ago but it needs to be maintained.  I'd also love to see similar mirrors for other repositories, e.g CRAN.</p>
<p>I believe it is important to pursue this during the pandemic because Citizen Lab and others confirm that COVID-19 researchers are being specifically targeted by state-sponsored hackers, and installing unsigned code is a too easy in to scientist's computers for the bad guys.  Recall also that one of the revelations from the Snowden leaks was the governments had specifically targeted climate change researchers, including the USG targeting foreign scientists and academics (with no "national security" ties) who were attending scientific conferences in the USA.</p>
<p>I hope TP can reach out to EFF (eff.org) and Sen. Ron Wyden (D-OR) with respect to opposing the EARN IT act, supporting federal grants for FOSS software and RFA (Radio Free Asia), opposing warrant less web surveillance, etc.  I believe that political threats to TP (especially in the USA) are just as important as the technical threats.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287890"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287890" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>debian user (not verified)</span> said:</p>
      <p class="date-time">May 18, 2020</p>
    </div>
    <a href="#comment-287890">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287890" class="permalink" rel="bookmark">Are you sure the debian…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are you sure the debian update mirrors aren't working? I used them recently and they worked fine for me...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287891"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287891" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>TorUserZ (not verified)</span> said:</p>
      <p class="date-time">May 18, 2020</p>
    </div>
    <a href="#comment-287891">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287891" class="permalink" rel="bookmark">Will 0.4.3.4rc be allowed in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Will 0.4.3.4rc be allowed in the wild for a while ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287893"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287893" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">May 18, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287891" class="permalink" rel="bookmark">Will 0.4.3.4rc be allowed in…</a> by <span>TorUserZ (not verified)</span></p>
    <a href="#comment-287893">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287893" class="permalink" rel="bookmark">Yup, we don&#039;t un-recommend…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yup, we don't un-recommend releases until there is a reason not to, and authorities don't start refusing relays running older versions unless they are seriously insecure or obsolete.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287898"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287898" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 19, 2020</p>
    </div>
    <a href="#comment-287898">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287898" class="permalink" rel="bookmark">5/15/20, 14:23:39.980 [WARN]…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>5/15/20, 14:23:39.980 [WARN] tor_bug_occurred_(): Bug: process_win32.c:891: process_win32_read_from_handle: Non-fatal assertion !(handle-&gt;reached_eof) failed. (on Tor 0.4.3.4-rc 251efd11e77df6c2)<br />
5/15/20, 14:23:39.980 [WARN] Bug: Tor 0.4.3.4-rc (git-251efd11e77df6c2): Non-fatal assertion !(handle-&gt;reached_eof) failed in process_win32_read_from_handle at process_win32.c:891. (Stack trace not available) (on Tor 0.4.3.4-rc 251efd11e77df6c2)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287906"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287906" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 19, 2020</p>
    </div>
    <a href="#comment-287906">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287906" class="permalink" rel="bookmark">Hi,
apparently you have…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,<br />
apparently you have forgotten to upload the armhf package on deb.torproject.org. </p>
<p>According to <a href="https://deb.torproject.org/torproject.org/dists/buster/main/binary-armhf/Packages" rel="nofollow">https://deb.torproject.org/torproject.org/dists/buster/main/binary-armh…</a> , 'tor-geoipdb' has been upgraded (0.4.3.5-1~d10.buster+1) whereas 'tor' has not (0.4.2.7-1~d10.buster+1). Would you upgrade and upload this latter as well, please?</p>
<p>Cheers</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287918"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287918" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">May 21, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287906" class="permalink" rel="bookmark">Hi,
apparently you have…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287918">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287918" class="permalink" rel="bookmark">Hi!  (My team doesn&#039;t…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!  (My team doesn't maintain the debian builds.)</p>
<p>It looks like the debian tor maintainer isn't able to do 32-bit arm builds any longer: <a href="https://lists.torproject.org/pipermail/tor-talk/2020-May/045582.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-talk/2020-May/045582.html</a> .  He suggests some alternatives in that thread, which is probably a good place to discuss it more?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288230"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288230" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>bcutter (not verified)</span> said:</p>
      <p class="date-time">June 09, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to nickm</p>
    <a href="#comment-288230">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288230" class="permalink" rel="bookmark">Can someone please explain…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can someone please explain what to do now? tor-geoipdb wants to update but tor itself can´t. Any other source providing 32-bit arm binaries?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288247"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288247" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 10, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to nickm</p>
    <a href="#comment-288247">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288247" class="permalink" rel="bookmark">I built it using this very…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I built it using this very useful ressource : <a href="https://wiki.debian.org/BuildingTutorial" rel="nofollow">https://wiki.debian.org/BuildingTutorial</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-287917"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287917" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 20, 2020</p>
    </div>
    <a href="#comment-287917">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287917" class="permalink" rel="bookmark">With the latest support for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>With the latest support for the v3 Onion balancing, when will the TP Hidden Services sites switch to v3?<br />
Any word on Debian going to v3?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287924"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287924" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Old Bill (not verified)</span> said:</p>
      <p class="date-time">May 22, 2020</p>
    </div>
    <a href="#comment-287924">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287924" class="permalink" rel="bookmark">The post says this release…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The post says this release has code for OnionBalance with v3 onion services support but this does not appear to be true. Only the ChangeLog and ReleaseNotes mention this but no code for onionbalance is in the release.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287986"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287986" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>dusk (not verified)</span> said:</p>
      <p class="date-time">May 30, 2020</p>
    </div>
    <a href="#comment-287986">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287986" class="permalink" rel="bookmark">so why is test version…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>so why is test version still0434rc ,windows expert bundle still 0427(friendly smile)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288108"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288108" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 03, 2020</p>
    </div>
    <a href="#comment-288108">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288108" class="permalink" rel="bookmark">In case anyone else was…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In case anyone else was looking for the changes since 0.4.2.6: <a href="https://gitweb.torproject.org/tor.git/plain/ReleaseNotes?h=tor-0.4.3.5" rel="nofollow">https://gitweb.torproject.org/tor.git/plain/ReleaseNotes?h=tor-0.4.3.5</a></p>
</div>
  </div>
</article>
<!-- Comment END -->

title: New Tor Browser Bundles
---
pub_date: 2011-06-25
---
author: erinn
---
tags:

tor browser bundle
alpha release
tbb
---
categories:

applications
releases
---
_html_body:

<p>All of the alpha Tor Browser Bundles have been updated to the latest Tor 0.2.2.29-beta.</p>

<p>Firefox 5 has recently been released and our next set of Firefox alpha bundles<br />
will come with that instead of Firefox 4. For users who want to use Firefox 5<br />
now, <a href="https://www.torproject.org/torbutton/" rel="nofollow">Torbutton 1.3.3-alpha</a> is compatible. </p>

<p>We're also going to begin phasing out the Firefox 3.6 bundles within the next<br />
month. Mike Perry is focusing his attention on the new Firefox releases (see<br />
his <a href="https://blog.torproject.org/blog/toggle-or-not-toggle-end torbutton" rel="nofollow">previous</a> <a href="http://blog.torproject.org/blog/improving-private-browsing-modes-do-not-track-vs-real-privacy-design" rel="nofollow">posts</a> on the topic) and we feel this is the best path to keep our users safe. You can also see his current Firefox patches in the <a href="https://gitweb.torproject.org/torbrowser.git/tree/refs/heads/maint-2.2:/src/current-patches" rel="nofollow">Tor Browser Bundle git repository</a>.</p>

<p><a href="https://torproject.org/download/download" rel="nofollow">https://torproject.org/download/download</a></p>

<p>The following changelogs encompass the would-be Tor 0.2.2.28-beta packages as<br />
well as the changes made for Tor 0.2.2.29-beta.</p>

<p><strong>Firefox 3.6 Tor Browser Bundles</strong></p>

<p><strong>OS X bundle</strong><br />
<strong>1.1.19:  Released 2011-06-21</strong></p>

<ul>
<li>Update Tor to 0.2.2.29-beta</li>
<li>Update NoScript to 2.1.1.1</li>
<li>Update HTTPS-Everywhere to 0.9.9.development.6</li>
</ul>

<p><strong>1.0.18: Released 2011-06-05</strong></p>

<ul>
<li>Update Tor to 0.2.2.28-beta</li>
<li>Update Libevent to 2.0.12-stable</li>
<li>Update zlib to 1.2.5</li>
<li>Update NoScript to 2.1.1</li>
<li>Update BetterPrivacy to 1.51</li>
</ul>

<p><strong>Linux bundles</strong><br />
<strong>1.1.11: Released 2011-06-21</strong></p>

<ul>
<li>Update Tor to 0.2.2.29-beta</li>
<li>Update NoScript to 2.1.1.1</li>
<li>Update HTTPS-Everywhere to 0.9.9.development.6</li>
</ul>

<p><strong>1.1.10: Released 2011-06-05</strong></p>

<ul>
<li>Update Tor to 0.2.2.28-beta</li>
<li>Update Libevent to 2.0.12-stable</li>
<li>Update zlib to 1.2.5</li>
<li>Update NoScript to 2.1.1</li>
<li>Update BetterPrivacy to 1.51</li>
</ul>

<p><strong>Firefox 4 Tor Browser Bundles</strong></p>

<p><strong>Tor Browser Bundle (2.2.29-1)</strong></p>

<ul>
<li>Update Tor to 0.2.2.29-beta</li>
<li>Update Libevent to 2.0.12-stable</li>
<li>Update HTTPS Everywhere to 0.9.9.development.6</li>
<li>Update NoScript to 2.1.1.1</li>
<li>Update BetterPrivacy to 1.51</li>
</ul>

<p>Temporary direct download links for Firefox 4 bundles:</p>

<ul>
<li><a href="https://torproject.org/dist/torbrowser/tor-browser-2.2.29-1-alpha_en-US.exe" rel="nofollow">Windows Tor Browser Bundle with Firefox 4</a> (<a href="https://torproject.org/dist/torbrowser/tor-browser-2.2.29-1-alpha_en-US.ex.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://torproject.org/dist/torbrowser/osx/TorBrowser-2.2.29-1-alpha-osx-x86_64-en-US.zip" rel="nofollow">OS X 64-bit Tor Browser Bundle with Firefox 4</a> (<a href="https://torproject.org/dist/torbrowser/osx/TorBrowser-2.2.29-1-alpha-osx-x86_64-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://torproject.org/dist/torbrowser/osx/TorBrowser-2.2.29-1-alpha-osx-i386-en-US.zip" rel="nofollow">OS X 32-bit Tor Browser Bundle with Firefox 4</a> (<a href="https://torproject.org/dist/torbrowser/osx/TorBrowser-2.2.29-1-alpha-osx-i386-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.2.29-1-alpha-en-US.tar.gz" rel="nofollow">GNU/Linux 64-bit Tor Browser Bundle with Firefox 4</a> (<a href="https://torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.2.29-1-alpha-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-i686-2.2.29-2-alpha-en-US.tar.gz" rel="nofollow">GNU/Linux 32-bit Tor Browser Bundle with Firefox 4</a> (<a href="https://torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-i686-2.2.29-2-alpha-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
</ul>


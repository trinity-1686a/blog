title: Tor 0.2.1.12-alpha is released
---
pub_date: 2009-02-09
---
author: phobos
---
tags:

bug fixes
alpha release
security fixes
improvements
---
categories: releases
---
_html_body:

<p>Tor 0.2.1.12-alpha features several more security-related fixes. You<br />
should upgrade, especially if you run an exit relay (remote crash) or<br />
a directory authority (remote infinite loop), or you're on an older<br />
(pre-XP) or not-recently-patched Windows (remote exploit). It also<br />
includes a big pile of minor bugfixes and cleanups.</p>

<p><a href="https://www.torproject.org/download.html.en" rel="nofollow">https://www.torproject.org/download.html.en</a></p>

<p>Changes in version 0.2.1.12-alpha - 2009-02-08<br />
<strong>Security fixes:</strong></p>

<ul>
<li>Fix an infinite-loop bug on handling corrupt votes under certain<br />
      circumstances. Bugfix on 0.2.0.8-alpha.</li>
<li>Fix a temporary DoS vulnerability that could be performed by<br />
      a directory mirror. Bugfix on 0.2.0.9-alpha; reported by lark.</li>
<li>Avoid a potential crash on exit nodes when processing malformed<br />
      input. Remote DoS opportunity. Bugfix on 0.2.1.7-alpha.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Let controllers actually ask for the "clients_seen" event for<br />
      getting usage summaries on bridge relays. Bugfix on 0.2.1.10-alpha;<br />
      reported by Matt Edman.</li>
<li>Fix a compile warning on OSX Panther. Fixes bug 913; bugfix against<br />
      0.2.1.11-alpha.</li>
<li>Fix a bug in address parsing that was preventing bridges or hidden<br />
      service targets from being at IPv6 addresses.</li>
<li>Solve a bug that kept hardware crypto acceleration from getting<br />
      enabled when accounting was turned on. Fixes bug 907. Bugfix on<br />
      0.0.9pre6.</li>
<li>Remove a bash-ism from configure.in to build properly on non-Linux<br />
      platforms. Bugfix on 0.2.1.1-alpha.</li>
<li>Fix code so authorities _actually_ send back X-Descriptor-Not-New<br />
      headers. Bugfix on 0.2.0.10-alpha.</li>
<li>Don't consider expiring already-closed client connections. Fixes<br />
      bug 893. Bugfix on 0.0.2pre20.</li>
<li>Fix another interesting corner-case of bug 891 spotted by rovv:<br />
      Previously, if two hosts had different amounts of clock drift, and<br />
      one of them created a new connection with just the wrong timing,<br />
      the other might decide to deprecate the new connection erroneously.<br />
      Bugfix on 0.1.1.13-alpha.</li>
<li>Resolve a very rare crash bug that could occur when the user forced<br />
      a nameserver reconfiguration during the middle of a nameserver<br />
      probe. Fixes bug 526. Bugfix on 0.1.2.1-alpha.</li>
<li>Support changing value of ServerDNSRandomizeCase during SIGHUP.<br />
      Bugfix on 0.2.1.7-alpha.</li>
<li>If we're using bridges and our network goes away, be more willing<br />
      to forgive our bridges and try again when we get an application<br />
      request. Bugfix on 0.2.0.x.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Support platforms where time_t is 64 bits long. (Congratulations,<br />
      NetBSD!) Patch from Matthias Drochner.</li>
<li>Add a 'getinfo status/clients-seen' controller command, in case<br />
      controllers want to hear clients_seen events but connect late.</li>
</ul>

<p><strong>Build changes:</strong></p>

<ul>
<li>Disable GCC's strict alias optimization by default, to avoid the<br />
      likelihood of its introducing subtle bugs whenever our code violates<br />
      the letter of C99's alias rules.</li>
</ul>

<p>The original announcement can be found at <a href="http://archives.seul.org/or/talk/Feb-2009/msg00054.html" rel="nofollow">http://archives.seul.org/or/talk/Feb-2009/msg00054.html</a></p>

---
_comments:

<a id="comment-609"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-609" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">February 09, 2009</p>
    </div>
    <a href="#comment-609">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-609" class="permalink" rel="bookmark">Any TBB updates coming for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any TBB updates coming for this version? if so when?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-611"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-611" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 10, 2009</p>
    </div>
    <a href="#comment-611">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-611" class="permalink" rel="bookmark">TOR will not install on OSX</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TOR will not install on OSX 10.5.6. I keep receiving an error at the end of install.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-667"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-667" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-611" class="permalink" rel="bookmark">TOR will not install on OSX</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-667">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-667" class="permalink" rel="bookmark">It installed, ignore Apple</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually, it did install.  What most likely failed is the automatic installation of torbutton into Firefox.  This is literally the last thing the installer does.  Everything else is installed fine, I bet.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-614"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-614" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://weddingfavourboxes.freehostingz.com">Anonymous (not verified)</a> said:</p>
      <p class="date-time">February 12, 2009</p>
    </div>
    <a href="#comment-614">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-614" class="permalink" rel="bookmark">What OS</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For what type of OS is it better?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-615"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-615" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2009</p>
    </div>
    <a href="#comment-615">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-615" class="permalink" rel="bookmark">ExitNodes {XX} can&#039;t work?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have tried and can't work... </p>
<p>BTW....where should i report this if I put this into a wrong post?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-666"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-666" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://bsd-tor-exit.dyndns.org">Randy McCallahan (not verified)</a> said:</p>
      <p class="date-time">February 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-615" class="permalink" rel="bookmark">ExitNodes {XX} can&#039;t work?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-666">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-666" class="permalink" rel="bookmark">bug reports to flyspray</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry, can't say anything to ExitNodes {X,Y,...}, but for bug reports exits a bug tracking system on <a href="https://bugs.torproject.org" rel="nofollow">https://bugs.torproject.org</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-670"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-670" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>SASI (not verified)</span> said:</p>
      <p class="date-time">February 15, 2009</p>
    </div>
    <a href="#comment-670">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-670" class="permalink" rel="bookmark">TBB updates coming? Please</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TBB updates coming? Please answer</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-672"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-672" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 16, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-670" class="permalink" rel="bookmark">TBB updates coming? Please</a> by <span>SASI (not verified)</span></p>
    <a href="#comment-672">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-672" class="permalink" rel="bookmark">Coming soon</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>yes, updates are coming..</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-673"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-673" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>SASI (not verified)</span> said:</p>
      <p class="date-time">February 16, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-673">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-673" class="permalink" rel="bookmark">thanx man ! Love 2 try it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanx man ! Love 2 try it</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-671"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-671" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Alex (not verified)</span> said:</p>
      <p class="date-time">February 15, 2009</p>
    </div>
    <a href="#comment-671">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-671" class="permalink" rel="bookmark">Updates on Tor data retention, JAP logging ?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are there any updates regarding Tor and German Data Retention ?<br />
I found only a relatively old update (<a href="https://blog.torproject.org/blog/tor%2C-germany%2C-and-data-retention" rel="nofollow">https://blog.torproject.org/blog/tor%2C-germany%2C-and-data-retention</a>).</p>
<p>I'm asking because some nodes in JAP implemented data retention recently: Here is what they log: (from <a href="http://anon.inf.tu-dresden.de/dataretention_en.html" rel="nofollow">http://anon.inf.tu-dresden.de/dataretention_en.html</a>)</p>
<p>=============================<br />
Therefore the Mixes of the AN.ON project will log the following data:<br />
1. A first Mix logs the IP-address, the date and time of incoming connections as well as the outgoing channel numbers of the channels to the next Mix.<br />
2. A middle Mix logs incoming and outgoing channel numbers as well as date and time of the channel openings.<br />
3. A last Mix logs the incoming channel numbers, the date and time of channel openings and closings, the source port number of outgoing connections as well as the date and time of openings of outgoing connections.<br />
...<br />
neither IP-addresses of contacted servers nor requested URLs will be logged.<br />
=============================</p>
<p>German version of the same text is here: <a href="http://anon.inf.tu-dresden.de/dataretention_de.html" rel="nofollow">http://anon.inf.tu-dresden.de/dataretention_de.html</a>. In JAP forum however, however, it was explained that TU Dresden implemented more than required by law.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-726"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-726" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous - no more (not verified)</span> said:</p>
      <p class="date-time">February 26, 2009</p>
    </div>
    <a href="#comment-726">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-726" class="permalink" rel="bookmark">Security issue still remains with this release</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>With the intoduction of version 2.1.6 Alpha you introduced a new seriouse security issue.<br />
If you are a Chinese who risk go to prison for your opinions or a Russian journalist who risk to be assasined or an Amerikan citisen who don't wan't the goverment to spy on them, you are not safe anymore until you fixed this issue with TOR.<br />
Even if people is not using the new country filter option in torrc...:<br />
Example:<br />
ExcludeNodes CN,GB,DE,US</p>
<p>...but only uses IP filters &amp; namefilters...:<br />
ExcludeNodes 0.0.0.0/5,147.0.0.0/8,111.111.111.111/32,jalopy,nixnix</p>
<p>..that worked up until version 2.1.5 Alpha this will not work properly anymore.<br />
TOR NOW USES NODES YOU BLOCK IN TORRC AS EXITNODES !!!<br />
This is a very seriouse security issue that you failed to fix in version 2.1.7, 2.1.8, 2.1.9, 2.1.10, 2.1.11, 2.1.12.<br />
How could you possibly miss to fix this issue?<br />
Is it done on purpose to serve demands from certain country(s) ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-733"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-733" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 27, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-726" class="permalink" rel="bookmark">Security issue still remains with this release</a> by <span>Anonymous - no more (not verified)</span></p>
    <a href="#comment-733">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-733" class="permalink" rel="bookmark">Report it?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If no one reports these issues, then it's difficult for us to find them.  We welcome help in improving our unit tests and feature testing.</p>
<p>We don't do any secret requests.  Everything we do is published via code commits (see or-cvs), blogged about, or tracked in the bug tracker.</p>
<p>If this has been going on for so long, I'm surprised no one else opened a bug tracker issue for it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-802" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 13, 2009</p>
    </div>
    <a href="#comment-802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-802" class="permalink" rel="bookmark">Actually, it did install.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually, it did install. What most likely failed is the automatic installation of torbutton into <a href="http://www.becomon.com" rel="nofollow">Firefox</a> . This is literally the last thing the installer does. Everything else is installed fine, I bet.</p>
</div>
  </div>
</article>
<!-- Comment END -->

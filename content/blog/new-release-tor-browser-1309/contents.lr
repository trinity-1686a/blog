title: New Release: Tor Browser 13.0.9
---
pub_date: 2024-01-22
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.9 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.9 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.9/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

This release updates Firefox to 115.7.0esr and Snowflake to 2.8.1. It also includes various bug fixes (see changelog for details).

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.8](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Snowflake to 2.8.1
  - [Bug tor-browser#42363](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42363): Tab thumbnails enhancements
  - [Bug tor-browser#42365](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42365): Rebase Tor Browser Stable onto Firefox 115.7.0esr
  - [Bug tor-browser#42367](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42367): Backport Android security fixes from Firefox 122
  - [Bug tor-browser#42369](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42369): Revert YEC 2023 changes
  - [Bug tor-browser-build#41058](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41058): Update Snowflake to 2.8.1
- Windows + macOS + Linux
  - Updated Firefox to 115.7.0esr
  - [Bug tor-browser#42099](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42099): Blind cross-origin requests to .tor.onion domains
  - [Bug tor-browser#42189](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42189): Assertion failure: the value of mPrivateBrowsingId in the loadContext and in the loadInfo are not the same!
- Android
  - Updated GeckoView to 115.7.0esr
  - [Bug tor-browser#42313](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42313): Enable One UI Sans KR as a possible font for Korean (MozBug 1865238)
  - [Bug tor-browser#42324](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42324): Onion Location on Android is ignored
  - [Bug tor-browser#42346](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42346): Crash in firefox-android originating in backported FullScreenNotificationDialog patch
  - [Bug tor-browser#42353](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42353): Fix Android NoScript automatic updates
  - [Bug tor-browser#42355](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42355): Fullscreen on Android doesn't hide system bars
- Build System
  - All Platforms
    - Updated Go to 1.20.13 and 1.21.6
    - [Bug tor-browser-build#41059](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41059): Update keyring/torbrowser.gpg with updated key
    - [Bug tor-browser-build#41063](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41063): Run "file $keyring" in tools/keyring/list-all-keyrings
  - Windows + macOS + Linux
    - [Bug tor-browser-build#41056](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41056): Make it possible to use templates in var/torbrowser_incremental_from
  - Windows + macOS
    - [Bug tor-browser-build#41016](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41016): Switch from bullseye to bookworm for desktop platforms
  - Windows
    - [Bug tor-browser-build#41015](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41015): Enable std::filesystem on libc++ on Windows
  - Android
    - [Bug tor-browser-build#41066](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41066): The Android x86 APK for 13.0.9 is too big

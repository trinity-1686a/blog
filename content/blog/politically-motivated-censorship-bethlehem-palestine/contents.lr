title: Politically motivated censorship in Bethlehem, Palestine
---
pub_date: 2012-04-23
---
author: art
---
tags:

censorship
ooniprobe
ooni
Palestine
---
categories:

circumvention
global south
---
_html_body:

<p>The internet agency Hadara is restricting access to certain content for users in Bethlehem, Palestine. The content of the websites in question appears to be in support of Muhammad Dahlan, the former leader of Fatah.</p>

<p>The Hadara network in Palestine is using a transparent HTTP proxy to inspect content and to censor the content of specific sites. A transparent HTTP proxy intercepts all of the customer's access attempts, technically the HTTP requests being done on the network by the customer's browser, and then the proxy makes requests in place of the client.  Sometimes this is a good thing, especially on congested networks or very expensive network connections. This is useful because the proxy can serve the cached version of the requested object if it already has the most up to date version of the requested page; therefore an ISP may minimize the amount of traffic that needs to leave it's network. Sometimes it's a bad thing, such as when the ISP censors access to an otherwise normally available resource.</p>

<p>We've found that the Hadara transparent HTTP proxy is not being used simply to decrease webpage loading times or to reduce costs. It is also being used to censor access and to block content. The <a href="http://ooni.nu" rel="nofollow">Open Observatory of Network Interference</a> (<a href="http://ooni.nu/" rel="nofollow">OONI</a>), did a scan of over 1 million websites to understand which ones where being censored and which were freely accessible.</p>

<p>The OONI scanning techniques involved connecting to a known good HTTP server and making HTTP GET requests. Each GET request contained specially crafted Host header field that contained a given possibly blocked website. These requests were intercepted by the transparent HTTP proxy and when the site was present inside of the blocklist, it would return an error page, rather than the expected content.</p>

<p>In each case of a blocked page, the error page would be served immediately (without requiring a connection to the outside network) it was possible to parallelize a large amount of simultaneous connections and to set a very low timeout value. Because of this we were able to probe access for the Alexa top 1 million domains in less than 7 days.</p>

<p>At the moment there has not been any official response from the ISP in question and the actors responsible for the censorship have not come out. We believe that the access to information is an intrinsic human right and measures that limit people ability to read should be fought.</p>

<p>For more technical details check out <a href="http://ooni.nu/releases/2012/Hadara_Palestine.html" rel="nofollow">the full report on the OONI webpage</a>.</p>

<p>For the journalistic stories relating to this filtering, please see <a href="http://www.maannews.net/eng/ViewDetails.aspx?ID=478726" rel="nofollow">the article on Ma'An</a> and <a href="https://www.eff.org/deeplinks/2012/04/palestinian-authority-found-block-critical-news-sites" rel="nofollow">this post on EFF</a>.</p>

---
_comments:

<a id="comment-15180"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15180" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 25, 2012</p>
    </div>
    <a href="#comment-15180">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15180" class="permalink" rel="bookmark">1 million domains Wow.. Hope</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>1 million domains Wow.. Hope u could do the same to scan Great Firewall of China</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15193"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15193" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 26, 2012</p>
    </div>
    <a href="#comment-15193">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15193" class="permalink" rel="bookmark">Hi I &#039;m new to Tor and have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi I 'm new to Tor and have a question (may be stupid one) but I just downloaded it and can see the browser is set to accept cookies. If I go to a website, my IP address is different, but when I press "show cookies" that website has put cookies on my computer. Can't the website see who I am, information about my computer or my real IP address with that cookie that is on MY computer and not on the exit node of the tor network???</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15423"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15423" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 04, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15193" class="permalink" rel="bookmark">Hi I &#039;m new to Tor and have</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15423">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15423" class="permalink" rel="bookmark">to test your anonymity use</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>to test your anonymity use these tools<br />
<a href="https://panopticlick.eff.org" rel="nofollow">https://panopticlick.eff.org</a><br />
<a href="http://ip-check.info" rel="nofollow">http://ip-check.info</a><br />
and follow their advices<br />
the cookies don't track you if you don't use them or delete them<br />
preferences&gt;privacy&gt;clear history when...</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-15194"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15194" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 26, 2012</p>
    </div>
    <a href="#comment-15194">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15194" class="permalink" rel="bookmark">While using Tor I did a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>While using Tor I did a security scan and it found several ports to be open:<br />
SSH   22<br />
HTTP  80<br />
SSL   443 </p>
<p>Is my privacy or security as risk because these ports are open when I do firewall test on tor browser?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15451"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15451" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 06, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15194" class="permalink" rel="bookmark">While using Tor I did a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15451">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15451" class="permalink" rel="bookmark">When you run a port scan</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When you run a port scan while running Tor, your Tor exit node is what gets scanned-- not your own system.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-15205"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15205" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 27, 2012</p>
    </div>
    <a href="#comment-15205">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15205" class="permalink" rel="bookmark">When I run TBB start script</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I run TBB start script it says in Terminal:</p>
<p>Qt: Session management error: None of the authentication protocols specified are supported</p>
<p>Please explain this, thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15422"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15422" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 04, 2012</p>
    </div>
    <a href="#comment-15422">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15422" class="permalink" rel="bookmark">tor will save the world</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>tor will save the world</p>
</div>
  </div>
</article>
<!-- Comment END -->

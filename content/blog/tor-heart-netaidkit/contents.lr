title: Tor at the Heart: NetAidKit
---
pub_date: 2016-12-26
---
author: ssteele
---
tags:

heart of Internet freedom
NetAidKit
---
_html_body:

<p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i><br />
<a href="https://torproject.org/donate/donate-blog26" rel="nofollow">Donate today!</a></p>

<h3>by Menso Heus</h3>

<p>The <a href="https://netaidkit.net/ " rel="nofollow">NetAidKit</a> is a USB-powered router that connects to your wired or wireless network and helps you increase your privacy and beat online censorship for all your devices. Acting as a friendly man-in-the-middle, the NetAidKit is able to send all your network traffic over a VPN or Tor connection without needing to configure any of your devices. This also means that if you have specific hardware devices that are unable to run Tor, you can simple connect them to the NetAidKit to make all the traffic go over Tor anyway.</p>

<p><a href="https://www.freepressunlimited.org/" rel="nofollow">Free Press Unlimited</a> and <a href="https://radicallyopensecurity.com/" rel="nofollow">Radically Open Security</a> developed the NetAidKit specifically for non-technical users, and the NetAidKit comes with an easy to use web interface that allows users to connect to Tor or upload OpenVPN configuration files and connect to VPN networks. </p>

<p>The NetAidKit transparently routes traffic over Tor. We believe this is a great (and free) way to circumvent censorship, but it obviously does not provide the same anonymity benefits that the Tor Browser Bundle provides. This is something we warn users about specifically every time they connect to Tor, recommending they also the Tor Browser Bundle if they wish to remain anonymous. </p>

<p>At the same time, by routing all traffic over Tor, NetAidKit provides a tool for users' e-mail, social media clients and other network applications to run over Tor as well, providing Tor's benefits to applications other than a browser.</p>

<p>The NetAidKit runs on <a href="https://openwrt.org/" rel="nofollow">OpenWRT</a> and uses the OpenWRT tor client. Current challenges include getting the obfuscating protocols to work on the NetAidKit since it has a limited storage capacity. We hope that in 2017 we can improve Tor support further by collaborating with the Tor Project. </p>

<p>For more information and links to our <a href="https://github.com/radicallyopensecurity/netaidkit" rel="nofollow">Github repository</a>, visit <a href="https://netaidkit.net/" rel="nofollow">https://netaidkit.net/</a></p>

---
_comments:

<a id="comment-228766"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228766" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 26, 2016</p>
    </div>
    <a href="#comment-228766">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228766" class="permalink" rel="bookmark">How is this different from</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How is this different from setting up a Raspberry Pi to route the wifi through Tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229802" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 03, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-228766" class="permalink" rel="bookmark">How is this different from</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229802" class="permalink" rel="bookmark">Off the top of my head,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Off the top of my head, without looking at the documentation,<br />
1. You don't need to buy a power cable, SD card, WiFi card (except Pi 3), or case<br />
2. You don't need to consume an extra USB port for power<br />
3. You don't need to install any OS on the SD card using another Linux machine or image writer application<br />
4. You don't need an HDMI compatible monitor/TV or serial port to perform the initial setup (enabling SSH)<br />
5. You don't need to install the Tor package, enable the systemd service, and edit torrc (to listen on non-loopback ports)<br />
6. You might not need to manually updates</p>
<p>Although personally I would just use a Raspberry Pi if I needed a dedicated hardware device for some reason, for many people it is easier to use one of these boxes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-228783"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228783" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 26, 2016</p>
    </div>
    <a href="#comment-228783">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228783" class="permalink" rel="bookmark">FOR  THE TOR DEVELOPERS: </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>FOR  THE TOR DEVELOPERS:  You might want to check if this new attack against elliptic curves might somehow affect Tor: <a href="https://ellipticnews.wordpress.com/2016/05/02/kim-barbulescu-variant-of-the-number-field-sieve-to-compute-discrete-logarithms-in-finite-fields/" rel="nofollow">https://ellipticnews.wordpress.com/2016/05/02/kim-barbulescu-variant-of…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229029"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229029" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  yawning
  </article>
    <div class="comment-header">
      <p class="comment__submitted">yawning said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-228783" class="permalink" rel="bookmark">FOR  THE TOR DEVELOPERS: </a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229029">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229029" class="permalink" rel="bookmark">This is offtopic to the post</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is offtopic to the post at hand, but Tor doesn't use Barreto-Naehrig curves or pairing base crypto.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-228798"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228798" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 26, 2016</p>
    </div>
    <a href="#comment-228798">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228798" class="permalink" rel="bookmark">Can&#039;t even read</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can't even read <a href="https://netaidkit.net/" rel="nofollow">https://netaidkit.net/</a> without loading JS.</p>
<p>That's a bad sign for a security/privacy project.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228821"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228821" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 26, 2016</p>
    </div>
    <a href="#comment-228821">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228821" class="permalink" rel="bookmark">This series is fabulous!
A</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This series is fabulous!</p>
<p>A few requests:</p>
<p>1. Keep a page collecting links to all the "Tor at the Heart" posts, for easy reference by pro-democracy enthusiasts who want to brag all next year to politicians about all the things Tor is doing for The People.</p>
<p>2. Summarize the results of the Funding Drive in pie charts as per Tails Project: where the money came from and where it is spent.</p>
<p>3. Ask Bruce Schneier or another expert to review the cryptographic state of the art at the layperson level (hard), with respect to technical threats and opportunities for future Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229030"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229030" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-228821" class="permalink" rel="bookmark">This series is fabulous!
A</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229030">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229030" class="permalink" rel="bookmark">For (1), check</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For (1), check out<br />
<a href="https://blog.torproject.org/category/tags/heart-internet-freedom" rel="nofollow">https://blog.torproject.org/category/tags/heart-internet-freedom</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-228962"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228962" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-228962">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228962" class="permalink" rel="bookmark">That&#039;s great ! What model of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's great ! What model of GLI device do you use ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-229253"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229253" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 29, 2016</p>
    </div>
    <a href="#comment-229253">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229253" class="permalink" rel="bookmark">I read through some of their</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I read through some of their code on github and I have to say I wasn't impressed... First thing I came across was their sshd running on a high port, which is a no-no for security. And all the actual options for hardening they could have used, they didn't implement. And then they have /usr/bin/netaidkit run with NOPASSWD sudo in their sudoers config, instead of using a service to run it as the proper user. Their password changing script hashes your password with... wait for it... MD5!</p>
<p>So yeah. Be skeptical.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232219"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232219" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 23, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229253" class="permalink" rel="bookmark">I read through some of their</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-232219">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232219" class="permalink" rel="bookmark">sshd port: Yeah, and why</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>sshd port: Yeah, and why exactly do you say that? Besides, it's running only if you compile a developer's image. Check the Makefile [1].</p>
<p>sudo: This daemon is doing things that need that. There always will be parts of your system that need that. Alternatives were considered. Security is not binary.</p>
<p>md5: It's a uClibc limitation in OpenWRT trunk and that's actually documented in repo history [2]. If anybody can physically access your router, you're screwed anyway and there's no escaping that. Always use a strong password and ideally never re-use it.</p>
<p>[1] <a href="https://github.com/radicallyopensecurity/netaidkit/blob/trunk/Makefile" rel="nofollow">https://github.com/radicallyopensecurity/netaidkit/blob/trunk/Makefile</a><br />
[2] <a href="https://github.com/radicallyopensecurity/netaidkit-nakd/commit/9b7569986016fdd070b857828126ef37596075a6" rel="nofollow">https://github.com/radicallyopensecurity/netaidkit-nakd/commit/9b756998…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-232332"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232332" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229253" class="permalink" rel="bookmark">I read through some of their</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-232332">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232332" class="permalink" rel="bookmark">If only uClibc would support</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If only uClibc would support it - they would use it. There has been rollback from SHA256 to MD5 due to that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-229798"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229798" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 03, 2017</p>
    </div>
    <a href="#comment-229798">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229798" class="permalink" rel="bookmark">Can NetAidKit or any other</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can NetAidKit or any other Tor box be easily configured to run as a plug and play relay?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-230033"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-230033" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 05, 2017</p>
    </div>
    <a href="#comment-230033">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-230033" class="permalink" rel="bookmark">Tor used to strongly</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor used to strongly discourage these "Tor router" boxes in the past. How is this one different?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-230050"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-230050" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 05, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-230033" class="permalink" rel="bookmark">Tor used to strongly</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-230050">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-230050" class="permalink" rel="bookmark">I think if we&#039;d had more</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think if we'd had more time, we would have done this blog post better. Netaidkit is a great candidate for Mike's upcoming "Tor Labs" plan, which aims to showcase projects that need more developer attention. In the mean time, for a bit more discussion about magic anonymity boxes, be sure to look at these two posts from the past:</p>
<p><a href="https://lists.torproject.org/pipermail/tor-relays/2014-October/005541.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-relays/2014-October/005541.h…</a></p>
<p><a href="https://lists.torproject.org/pipermail/tor-relays/2014-October/005544.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-relays/2014-October/005544.h…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-232224"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232224" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 23, 2017</p>
    </div>
    <a href="#comment-232224">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232224" class="permalink" rel="bookmark">I have one, and it works.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have one, and it works.</p>
</div>
  </div>
</article>
<!-- Comment END -->
